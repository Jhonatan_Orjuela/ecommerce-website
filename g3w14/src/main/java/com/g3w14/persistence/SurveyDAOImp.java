package com.g3w14.persistence;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.SurveyBean;

/**
 * @author 1040134 Jhonatan Orjuela
 * 
 */
@Named("surveyAction")
@RequestScoped
public class SurveyDAOImp implements SurveyDAO, Serializable {

	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;

	/**
	 * No-parameter constructor.
	 */
	public SurveyDAOImp() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.SurveyDAO#surveyRecords()
	 */
	@Override
	public ArrayList<SurveyBean> getSurveyRecords() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String sql = "SELECT * FROM survey";
		ArrayList<SurveyBean> rows = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet resultSet = pStatement.executeQuery()) {
			while (resultSet.next()) {
				SurveyBean sB = new SurveyBean();
				sB.setSurveyId(resultSet.getInt("surveyid"));
				sB.setQuestion(resultSet.getString("question"));
				sB.setOption1(resultSet.getString("option1"));
				sB.setVotes1(resultSet.getInt("votes1"));
				sB.setOption2(resultSet.getString("option2"));
				sB.setVotes2(resultSet.getInt("votes2"));
				sB.setOption3(resultSet.getString("option3"));
				sB.setVotes3(resultSet.getInt("votes3"));
				sB.setOption4(resultSet.getString("option4"));
				sB.setVotes4(resultSet.getInt("votes4"));
				sB.setTotalVotes(resultSet.getInt("totalvotes"));
				sB.setDisable(resultSet.getBoolean("disable"));
				rows.add(sB);
			}
		}

		return rows;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.SurveyDAO#extractSingleSurvey(int)
	 */
	@Override
	public SurveyBean getSingleSurvey(int surveyId) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
//		System.out.println(surveyId);
		SurveyBean sB = new SurveyBean();
		String sql = "SELECT * FROM survey WHERE surveyid = ?";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pS = connection.prepareStatement(sql);
				) {

			pS.setInt(1, surveyId);
			ResultSet resultSet = pS.executeQuery();
			while(resultSet.next()){
				sB.setSurveyId(resultSet.getInt("surveyid"));
				sB.setQuestion(resultSet.getString("question"));
				sB.setOption1(resultSet.getString("option1"));
				sB.setVotes1(resultSet.getInt("votes1"));
				sB.setOption2(resultSet.getString("option2"));
				sB.setVotes2(resultSet.getInt("votes2"));
				sB.setOption3(resultSet.getString("option3"));
				sB.setVotes3(resultSet.getInt("votes3"));
				sB.setOption4(resultSet.getString("option4"));
				sB.setVotes4(resultSet.getInt("votes4"));
				sB.setTotalVotes(resultSet.getInt("totalvotes"));
				sB.setDisable(resultSet.getBoolean("disable"));
			}
		}
//		System.out.println(sB.getDisable());
		return sB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.g3w14.persistence.SurveyDAO#insertSurvey(com.g3w14.data.SurveyBean)
	 */
	@Override
	public int insertSurvey(SurveyBean survey) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int records = -1;

		String insertStmt = "INSERT INTO survey(question, option1, votes1,"
				+ " option2, votes2, option3, votes3"
				+ ", option4, votes4, totalvotes,disable) " + "VALUES(?,?,?,?,?,?,?,?,?,?,?)";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pS = connection
						.prepareStatement(insertStmt);) {
			pS.setString(1, survey.getQuestion());
			pS.setString(2, survey.getOption1());
			pS.setInt(3, survey.getVotes1());
			pS.setString(4, survey.getOption2());
			pS.setInt(5, survey.getVotes2());
			pS.setString(6, survey.getOption3());
			pS.setInt(7, survey.getVotes3());
			pS.setString(8, survey.getOption4());
			pS.setInt(9, survey.getVotes4());
			pS.setInt(10, survey.getTotalVotes());
			pS.setBoolean(11, survey.getDisable());
			records = pS.executeUpdate();

		}
		return records;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.g3w14.persistence.SurveyDAO#updateSurvey(com.g3w14.data.SurveyBean)
	 */
	@Override
	public int updateSurvey(SurveyBean updatedSurvey) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int records = -1;
		String updateStatement = "UPDATE survey SET question = ?,"+
					"option1 = ?, votes1 = ?, option2 = ?, votes2 = ?, option3 = ?, votes3 = ?, option4 = ?, votes4 = ?, totalvotes = ?, disable = ? WHERE surveyid = ?";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pS = connection
						.prepareStatement(updateStatement);) {
			
			pS.setString(1, updatedSurvey.getQuestion());
			pS.setString(2, updatedSurvey.getOption1());
			pS.setInt(3, updatedSurvey.getVotes1());
			pS.setString(4, updatedSurvey.getOption2());
			pS.setInt(5, updatedSurvey.getVotes2());
			pS.setString(6, updatedSurvey.getOption3());
			pS.setInt(7, updatedSurvey.getVotes3());
			pS.setString(8, updatedSurvey.getOption4());
			pS.setInt(9, updatedSurvey.getVotes4());
			pS.setInt(10, updatedSurvey.getTotalVotes());
			pS.setBoolean(11, updatedSurvey.getDisable());
			pS.setInt(12, updatedSurvey.getSurveyId());
			
			records = pS.executeUpdate();

		}
		return records;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.g3w14.persistence.SurveyDAO#deleteSurvey(com.g3w14.data.SurveyBean)
	 */
	@Override
	public int deleteSurvey(SurveyBean survey) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int records = -1;

		String deleteStatement = "DELETE FROM survey " + "WHERE surveyid = ?";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pS = connection
						.prepareStatement(deleteStatement);) {

			pS.setInt(1, survey.getSurveyId());

			records = pS.executeUpdate();
		}

		return records;
	}
}