/**
 * 
 */
package com.g3w14.persistence;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.ShippingInfoBean;

/**
 * @author Jhonatan Orjuela
 * @author Sophie Leduc Major
 * 
 */
@Named("shippingInfo")
@RequestScoped
public class ShippingInfoDAOImp implements Serializable {

	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;

	/**
	 * 
	 */
	public ShippingInfoDAOImp() {
		super();
	}

	public ArrayList<ShippingInfoBean> getQueryRecords() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source");

		ArrayList<ShippingInfoBean> shipInfoList = new ArrayList<>();
		String sql = "SELECT * FROM shipinfo";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);
				ResultSet resultSet = ps.executeQuery()) {
			while (resultSet.next()) {
				ShippingInfoBean ship = new ShippingInfoBean();

				ship.setShipName(resultSet.getString("shipname"));
				ship.setShipTitle(resultSet.getString("shiptitle"));
				ship.setShipLastName(resultSet.getString("shiplastname"));
				ship.setShipFirstName(resultSet.getString("shipfirstname"));
				ship.setShipCompany(resultSet.getString("shipcompany"));
				ship.setShipAddress1(resultSet.getString("shipaddress1"));
				ship.setShipAddress2(resultSet.getString("shipaddress2"));
				ship.setShipCity(resultSet.getString("shipcity"));
				ship.setShipProvince(resultSet.getString("shipprovince"));
				ship.setShipCountry(resultSet.getString("shipcountry"));
				ship.setShipPostalCode(resultSet.getString("shippostalcode"));
				ship.setClientid(resultSet.getInt("clientid"));
				shipInfoList.add(ship);
			}
		}
		return shipInfoList;
	}

	public ArrayList<ShippingInfoBean> searchShipInfoByClientId(int id)
			throws SQLException {

		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source");

		ArrayList<ShippingInfoBean> shipInfoList = new ArrayList<>();
		String sql = "SELECT * FROM shipinfo WHERE clientid = ?";
		ShippingInfoBean ship;

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);

		) {

			ps.setInt(1, id);
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				ship = new ShippingInfoBean();
				ship.setId(resultSet.getInt("id"));
				ship.setShipName(resultSet.getString("shipname"));
				ship.setShipTitle(resultSet.getString("shiptitle"));
				ship.setShipLastName(resultSet.getString("shiplastname"));
				ship.setShipFirstName(resultSet.getString("shipfirstname"));
				ship.setShipCompany(resultSet.getString("shipcompany"));
				ship.setShipAddress1(resultSet.getString("shipaddress1"));
				ship.setShipAddress2(resultSet.getString("shipaddress2"));
				ship.setShipCity(resultSet.getString("shipcity"));
				ship.setShipProvince(resultSet.getString("shipprovince"));
				ship.setShipCountry(resultSet.getString("shipcountry"));
				ship.setShipPostalCode(resultSet.getString("shippostalcode"));
				ship.setClientid(resultSet.getInt("clientid"));

				shipInfoList.add(ship);
			}
			
			
		}

		return shipInfoList;
	}
	public ShippingInfoBean searchShipInfoById(int id)
			throws SQLException {

		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source");

		ArrayList<ShippingInfoBean> shipInfoList = new ArrayList<>();
		String sql = "SELECT * FROM shipinfo WHERE id = ?";
		ShippingInfoBean ship;

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);

		) {

			ps.setInt(1, id);
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				ship = new ShippingInfoBean();
				ship.setId(resultSet.getInt("id"));
				ship.setShipName(resultSet.getString("shipname"));
				ship.setShipTitle(resultSet.getString("shiptitle"));
				ship.setShipLastName(resultSet.getString("shiplastname"));
				ship.setShipFirstName(resultSet.getString("shipfirstname"));
				ship.setShipCompany(resultSet.getString("shipcompany"));
				ship.setShipAddress1(resultSet.getString("shipaddress1"));
				ship.setShipAddress2(resultSet.getString("shipaddress2"));
				ship.setShipCity(resultSet.getString("shipcity"));
				ship.setShipProvince(resultSet.getString("shipprovince"));
				ship.setShipCountry(resultSet.getString("shipcountry"));
				ship.setShipPostalCode(resultSet.getString("shippostalcode"));
				ship.setClientid(resultSet.getInt("clientid"));

				shipInfoList.add(ship);
			}
			
			
		}

		return shipInfoList.get(0);
	}

	public int insertShippingInfo(ShippingInfoBean ship) throws SQLException {

		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		int records = -1;

		String insertStmt = "INSERT INTO shipinfo(shipname, shiptitle, shiplastname, shipfirstname, shipcompany,"
				+ " shipaddress1, shipaddress2, shipcity, shipprovince, shipcountry, shippostalcode, clientid) "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(insertStmt);) {
			ps.setString(1, ship.getShipName());
			ps.setString(2, ship.getShipTitle());
			ps.setString(3, ship.getShipLastName());
			ps.setString(4, ship.getShipFirstName());
			ps.setString(5, ship.getShipCompany());
			ps.setString(6, ship.getShipAddress1());
			ps.setString(7, ship.getShipAddress2());
			ps.setString(8, ship.getShipCity());
			ps.setString(9, ship.getShipProvince());
			ps.setString(10, ship.getShipCountry());
			ps.setString(11, ship.getShipPostalCode());
			ps.setInt(12, ship.getClientid());

			records = ps.executeUpdate();
		}

		return records;
	}

	public int updateShipInfo(ShippingInfoBean ship) throws SQLException {

		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		int records = -1;
		System.out.println("$$$$$$" + ship.getClientid());
		String updateStmt = "UPDATE shipinfo SET " + "shipname = ?,"
				+ "shiptitle = ?," + "shiplastname = ?," + "shipfirstname = ?,"
				+ "shipcompany = ?," + "shipaddress1 = ?," + "shipaddress2 = ?,"
				+ "shipcity = ?," + "shipprovince = ?," + "shipcountry = ?,"
				+ "shippostalcode = ?" + " WHERE id = ?";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(updateStmt);) {
			ps.setString(1, ship.getShipName());
			ps.setString(2, ship.getShipTitle());
			ps.setString(3, ship.getShipLastName());
			ps.setString(4, ship.getShipFirstName());
			ps.setString(5, ship.getShipCompany());
			ps.setString(6, ship.getShipAddress1());
			ps.setString(7, ship.getShipAddress2());
			ps.setString(8, ship.getShipCity());
			ps.setString(9, ship.getShipProvince());
			ps.setString(10, ship.getShipCountry());
			ps.setString(11, ship.getShipPostalCode());
			ps.setInt(12, ship.getId());

			records = ps.executeUpdate();
		}

		return records;
	}

	public int deleteShipInfo(ShippingInfoBean ship) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		System.out.println("DELETE SIPINFO");
		System.out.println(ship.getId());
		int records = -1;

		String deleteStatement = "DELETE FROM shipinfo " + "WHERE id = ?";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pS = connection
						.prepareStatement(deleteStatement);) {

			pS.setInt(1, ship.getId());

			records = pS.executeUpdate();
		}

		return records;
	}

}
