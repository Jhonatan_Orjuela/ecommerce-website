package com.g3w14.persistence;

import java.sql.SQLException;
import java.util.ArrayList;

import com.g3w14.data.TaxBean;

/**
 * Mandates the function of Tax Data Access Objects to query the records
 * in the Tax table along with other forms of data manipulation.
 * 
 * @author Tyler Patricio
 * @author Sophie Leduc Major
 */
public interface TaxDAO
{
	public ArrayList<TaxBean> returnAllRecords() throws SQLException;
	//public ArrayList<TaxBean> advancedSearch(TaxBean tb) throws SQLException; useless, we will never search a tax like that - Jhon
	public int deleteRecord(int id) throws SQLException;
	public int insertRecord(TaxBean tb) throws SQLException;
	public int updateRecord(TaxBean tb) throws SQLException;
	public TaxBean getTaxById(int id) throws SQLException;
	public TaxBean getTaxByProvince(String province) throws SQLException;
}