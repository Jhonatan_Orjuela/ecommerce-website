package com.g3w14.persistence;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.ClientBean;
import com.g3w14.data.TaxBean;

/**
 * Implementation class that enables the ability to manipulate and query records from 
 * the taxes table.
 * 
 * @author Tyler Patricio
 * @author Sophie Leduc Major
 */
@Named("taxAction")
@RequestScoped
public class TaxDAOImp implements TaxDAO, Serializable
{
	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;
	
	/**
	 * No-parameter constructor.
	 */
	public TaxDAOImp() 
	{
		super();
	}
	
	/**
	 * Acquires all the records from the taxes table.
	 * 
	 * @return the ArrayList containing the taxes data.
	 */
	@Override
	public ArrayList<TaxBean> returnAllRecords() throws SQLException
	{
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		ArrayList<TaxBean> rows = new ArrayList<>();
		String sql = "SELECT * FROM taxes";
		
		//try with resources block
		try (Connection connection = dataSource.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sql);
			ResultSet resultSet = pStatement.executeQuery()) 
		{
			while (resultSet.next())
			{
				TaxBean taxBean = new TaxBean();

				taxBean.setProvince(resultSet.getString("province"));
				taxBean.setPst(resultSet.getBigDecimal("pst"));

				rows.add(taxBean);
			}
		}
		
		return rows;
	}

	/**
	 * Acquires the desired records based on a query String.
	 *       
	 * @param tb	data used to generate the SQL statement.         
	 * @return 		the ArrayList containing the taxes data based on the query.
	 */
	/*
	@Override
	public ArrayList<TaxBean> advancedSearch(TaxBean tb) throws SQLException
	{
		ArrayList<TaxBean> rows = new ArrayList<>();
		ArrayList<Object> queryInfo = new ArrayList<>();
	
		queryInfo = sqlMaker(tb);
		
		// variable used to hold size of ArrayList for efficiency of looping condition
		int size = queryInfo.size() - 1;
		
		String sql = (String)queryInfo.get(size);
		System.out.println("¨¨¨¨¨¨¨¨¨¨¨¨¨" + sql);
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
				PreparedStatement pStatement = connection.prepareStatement(sql);)
		{
			for (int i = 0; i < size; i++)
			{
				// check fields for specific class to properly set values in prepared statement
				if (queryInfo.get(i) instanceof Integer)
					pStatement.setInt(i + 1, (int)queryInfo.get(i));
				
				else if (queryInfo.get(i) instanceof String)
					pStatement.setString(i + 1, (String)queryInfo.get(i));
				
				else if (queryInfo.get(i) instanceof BigDecimal)
					pStatement.setBigDecimal(i + 1, (BigDecimal)queryInfo.get(i));
			}

			try (ResultSet resultSet = pStatement.executeQuery();)
			{
				while (resultSet.next())
				{
					TaxBean taxBean = new TaxBean();

					taxBean.setProvince(resultSet.getString("province"));
					taxBean.setPst(resultSet.getBigDecimal("pst"));

					rows.add(taxBean);
				}
			} //end inner-try
		}

		return rows;
	}
*/
	
	/**
	 * Acquires the desired records based on a query String.
	 *       
	 * @param tb	data used to generate the SQL statement.         
	 * @return 		the ArrayList containing the taxes data based on the query.
	 * @author Sophie Leduc Major
	 */
	@Override
	public TaxBean getTaxById(int id) throws SQLException
	{
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String sql = "SELECT * FROM taxes WHERE id = ?";
		ArrayList<TaxBean> rows = new ArrayList<TaxBean>();

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setInt(1, id);

			try (ResultSet resultSet = pStatement.executeQuery();) {
				while (resultSet.next()) {
					TaxBean taxBean = new TaxBean();

					taxBean.setProvince(resultSet.getString("province"));
					taxBean.setPst(resultSet.getBigDecimal("pst"));

					rows.add(taxBean);
				}
			}
		}

		if(rows.size()!=1)
			rows.add(null);

		return rows.get(0);
	}
	
	/**
	 * Acquires the desired records based on a query String.
	 *       
	 * @param tb	data used to generate the SQL statement.         
	 * @return 		the ArrayList containing the taxes data based on the query.
	 * @author Sophie Leduc Major
	 */
	@Override
	public TaxBean getTaxByProvince(String province) throws SQLException
	{
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String sql = "SELECT * FROM taxes WHERE province = ?";
		ArrayList<TaxBean> rows = new ArrayList<TaxBean>();

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setString(1, province);

			try (ResultSet resultSet = pStatement.executeQuery();) {
				while (resultSet.next()) {
					TaxBean taxBean = new TaxBean();
					
					taxBean.setId(resultSet.getInt("id"));
					taxBean.setProvince(resultSet.getString("province"));
					taxBean.setPst(resultSet.getBigDecimal("pst"));

					rows.add(taxBean);
				}
			}
		}

		if(rows.size()!=1)
			rows.add(null);

		return rows.get(0);
	}
	
	/**
	 * Deletes a specified record from the taxes table.
	 *            
	 * @return the counter value for the newly deleted record (either 0 or 1)
	 */
	@Override
	public int deleteRecord(int id) throws SQLException
	{
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		// record counter for newly deleted row
		int record;

		String sql = "DELETE FROM taxes WHERE id = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);)
				{
			pStatement.setInt(1, id);
			record = pStatement.executeUpdate();
				}

		return record;
	}

	/**
	 * Inserts a specified record into the taxes table.
	 *            
	 * @return the counter value for the newly inserted record (either 0 or 1)
	 */
	@Override
	public int insertRecord(TaxBean tb) throws SQLException
	{
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		// record counter for newly added row
		int record;

		String preparedQuery = "INSERT INTO taxes (province, pst) VALUES (?, ?);";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(preparedQuery);)
				{
			pStatement.setString(1, tb.getProvince());
			pStatement.setBigDecimal(2, tb.getPst());

			record = pStatement.executeUpdate();
				}

		return record;
	}

	/**
	 * Updates specified records from the taxes table.
	 *            
	 * @return the counter value for the newly updated record (either 0 or 1)
	 */
	@Override
	public int updateRecord(TaxBean tb) throws SQLException
	{
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		int record;
		String preparedSQL = "UPDATE taxes SET province = ?, pst = ? WHERE id = ?";
		
		try (Connection connection = dataSource.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(preparedSQL);)
		{
			pStatement.setString(1, tb.getProvince());
			pStatement.setBigDecimal(2, tb.getPst());
			pStatement.setInt(3, tb.getId());

			record = pStatement.executeUpdate();
		}
		
		return record;
	}
	
	/**
	 * Generates the SQL statement for searching by a specific query.
	 *            
	 * @param tb	data used to generate the SQL statement
	 * @return 		the list containing not null field values along with
	 * 		   		the newly constructed SQL statement
	 */
	private ArrayList<Object> sqlMaker(TaxBean tb)
	{
		ArrayList<Object> queryInfo = new ArrayList<>();

		String sql = "SELECT * FROM taxes WHERE";
		
		if (tb.getId() != 0)
		{
			queryInfo.add(tb.getId());
			sql += " id = ? AND";
		}
		
		if (tb.getProvince().length() != 0)
		{
			queryInfo.add(tb.getProvince());
			sql += " province = ? AND";
		}
			
		if (tb.getPst() != null && tb.getPst().compareTo(new BigDecimal(-1.00)) > 0)
		{
			queryInfo.add(tb.getPst());
			sql += " pst = ? AND";
		}
		
		sql = sql.substring(0, sql.length() - 4);
		queryInfo.add(sql);

		return queryInfo;
	}
}