package com.g3w14.persistence;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;

import com.g3w14.data.InvoiceBean;

/**
 * Mandates the function of Invoice Data Access Objects to query the records
 * in the Invoice table along with other forms of data manipulation.
 * 
 * @author 1410616
 *
 */
public interface InvoiceDAO 
{
	/**
	 * extracts the full list of invoices
	 * @return The list of extracted invoices
	 * @throws SQLException
	 */
	public ArrayList<InvoiceBean> getQueryRecords() throws SQLException;
	/**
	 * extracts the full list of invoices in date range
	 * @return The list of extracted invoices
	 * @throws SQLException
	 */
	public ArrayList<InvoiceBean> getQueryRecordsDate(String date1, String date2) throws SQLException;
	/**
	 * extracts a single invoice
	 * @param invoiceId the id of the invoice
	 * @return the invoice extracted
	 * @throws SQLException
	 */
	public InvoiceBean extractSingleInvoice(int invoiceId) throws SQLException;
	/**
	 * Insert an invoice in the Database
	 * @param invb the invoice to be inserted
	 * @return the number of records inserted
	 * @throws SQLException
	 */
	public int insertRecord(InvoiceBean invb) throws SQLException;
	/**
	 * Updates an invoice in the Database
	 * @param invb the invoice to update
	 * @return the number of records altered
	 * @throws SQLException
	 */
	public int updateRecord(InvoiceBean invb) throws SQLException;
	/**
	 * deletes an invoice in the database
	 * @param salenb the invoice to delete
	 * @return the number of records updates
	 * @throws SQLException
	 */
	public int deleteRecord(InvoiceBean ib) throws SQLException;
	/**
	 * extracts the full list of invoices of a client
	 * @param client number
	 * @return the number of records updates
	 * @throws SQLException
	 */
	public ArrayList<InvoiceBean> getInvoiceClient(String clientnumber, String date1, String date2) throws SQLException;

	/**
	 * 
	 * @return the total purchase by a client
	 * @throws SQLException
	 */
	public BigDecimal getTotalGross(String clientnumber) throws SQLException;
	/**
	 * 
	 * @return the total purchase for bookbay
	 * @throws SQLException
	 */
	public BigDecimal getTotalGrossBookbay() throws SQLException;
	/**
	 * 
	 * @return the total purchase for bookbay
	 * @throws SQLException
	 */
	public ArrayList<InvoiceBean> getTopClients(String date1, String date2) throws SQLException;
}
