package com.g3w14.persistence;

import java.sql.SQLException;
import java.util.ArrayList;

import com.g3w14.data.InvoiceDetailBean;
/**
 * @author 1410616
 *
 */
public interface InvoiceDetailDAO 
{
	/**
	 * extracts the full list of invoicedetails
	 * @return The list of extracted invoicedetails
	 * @throws SQLException
	 */
	public ArrayList<InvoiceDetailBean> getQueryRecords() throws SQLException;
	/**
	 * extracts the full list of invoicedetails of one author
	 * @return The list of extracted invoicedetails
	 * @throws SQLException
	 */
	public ArrayList<InvoiceDetailBean> getQueryRecordsByAuthor(String author, String date1, String date2) throws SQLException;
	/**
	 * extracts the full list of invoicedetails of one publisher
	 * @return The list of extracted invoicedetails
	 * @throws SQLException
	 */
	public ArrayList<InvoiceDetailBean> getQueryRecordsByPublisher(String publisher, String date1, String date2) throws SQLException;
	/**
	 * extracts the full list of topsellers
	 * @return The list of extracted invoicedetails
	 * @throws SQLException
	 */
	public ArrayList<InvoiceDetailBean> getTopSellers(String date1, String date2) throws SQLException;
	/**
	 * extracts all the invociedetail of one invoice
	 * @return The list of extracted invoicedetails
	 * @throws SQLException
	 */
	public ArrayList<InvoiceDetailBean> getInvoiceDetailPerInvoice(int invoicenumber) throws SQLException;
	/**
	 * extracts a single invoicedetail
	 * @param invoicecId the id of the invoicedetail
	 * @return the invoicedetail extracted
	 * @throws SQLException
	 */
	public InvoiceDetailBean extractSingleInvoiceDetail(int invoicedId) throws SQLException;
	/**
	 * Insert an invoicedetail in the Database
	 * @param incvb the invoicedetail to be inserted
	 * @return the number of records inserted
	 * @throws SQLException
	 */
	public int insertRecord(InvoiceDetailBean indvb) throws SQLException;
	/**
	 * Updates an invoicedetail in the Database
	 * @param incvb the invoicedetail to update
	 * @return the number of records altered
	 * @throws SQLException
	 */
	public int updateRecord(InvoiceDetailBean indvb) throws SQLException;
	/**
	 * deletes an invoicedetail in the database
	 * @param id the invoicedetail to delete
	 * @return the number of records updates
	 * @throws SQLException
	 */
	public int deleteRecord(InvoiceDetailBean indvb) throws SQLException;
}