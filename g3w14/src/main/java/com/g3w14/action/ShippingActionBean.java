package com.g3w14.action;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.ClientBean;
import com.g3w14.data.ShippingInfoBean;
import com.g3w14.persistence.ShippingInfoDAOImp;

/**
 * ShippingActionBean
 * 
 * This bean manages the shipping information for the user.
 * 
 * 
 * @author Sophie Leduc Major
 */
@SuppressWarnings("serial")
@Named("shippingActionBean")
@ConversationScoped
public class ShippingActionBean implements Serializable {

	@Inject
	private ShippingInfoBean sib;
	@Inject
	private ShippingInfoDAOImp sidi;
	@Inject
	private LoginBean lb;
	@Inject
	private ClientBean cb;

	/**
	 * locationValue
	 * 
	 * The lacation value is the shipping name of the place the user wants to
	 * ship it to. The name is based off the shipping information. The name is
	 * ganareated using the shipping first name, the last name and the first
	 * address. eg: ("Sophie Leduc, 123 Road" would be my shipName for this
	 * location.)
	 * 
	 */
	private static Map<String, Object> locationValue = new LinkedHashMap<String, Object>();

	/**
	 * shipinfos This contains all of the shipping entry the user has.
	 * 
	 */
	ArrayList<ShippingInfoBean> shipinfos = new ArrayList<ShippingInfoBean>();

	public Map<String, Object> getlocationValue() {

		return locationValue;
	}

	public static void setLocationValue(Map<String, Object> locationValue) {
		ShippingActionBean.locationValue = locationValue;
	}

	public ArrayList<ShippingInfoBean> getShipinfos() {
		return shipinfos;
	}

	public void setShipinfos(ArrayList<ShippingInfoBean> shipinfos) {
		this.shipinfos = shipinfos;
	}

	public ShippingActionBean() {
		sib = new ShippingInfoBean();
		sidi = new ShippingInfoDAOImp();

	}

	/**		generateLocations
	 * This method makes the drop down menus items using the shipping names as values.
	 * 
	 * @throws SQLException
	 */
	public void generateLocations() throws SQLException {
		locationValue.put("", "");

		shipinfos = sidi.searchShipInfoByClientId(lb.getClientBean().getId());

		for (int i = 0; i < shipinfos.size(); i++) {
			locationValue.put(shipinfos.get(i).getShipName(), shipinfos.get(i));
		}
		cb = new ClientBean();
		cb = lb.getClientBean();

	}

	/**		locationChange
	 * 
	 * When the user selects an address from the menu, this sets the current shipping bean
	 * to that address. 
	 * 
	 * @param event
	 * @throws SQLException
	 */
	public void locationChange(ValueChangeEvent event) throws SQLException {

		//get all the shipping info entries 
		shipinfos = sidi.searchShipInfoByClientId(lb.getClientBean().getId());

		
		//look for which one the user selected.
		for (int i = 0; i < shipinfos.size(); i++) {
			if (shipinfos.get(i).getShipName().equals(event.getNewValue())) {
				
				//set the bean
				sib = shipinfos.get(i);
				
				//Refresh the page
				refresh();

			}
		}

	}
	
	
	/**		shipInformation
	 * When the user has a new shipping location, a shipping name is generated here.
	 * The shipping game is his first name followed by his last anme and his first address
	 * 
	 * @throws SQLException
	 */
	public void shipInformation() throws SQLException {
		if (sib.getShipName() == "") {
			
			//looks like this: 'Firstname Lastname ,Address1'
			sib.setShipName(sib.getShipFirstName() + " "
					+ sib.getShipLastName() + " ," + sib.getShipAddress1());
			sib.setClientid(lb.getClientBean().getId());
			sidi.insertShippingInfo(sib);
		}
	}

	/**		refresh
	 * This refreshes the page.
	 * 
	 */
	private void refresh() {
		FacesContext c = FacesContext.getCurrentInstance();
		String viewId = c.getViewRoot().getViewId();
		ViewHandler h = c.getApplication().getViewHandler();
		UIViewRoot r = h.createView(c, viewId);
		r.setViewId(viewId);
		c.setViewRoot(r);
	}

}
