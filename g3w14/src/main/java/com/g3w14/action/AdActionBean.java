package com.g3w14.action;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.AdvertisementBean;
import com.g3w14.persistence.AdvertisementDAOImp;

/**
 * Action Bean class that maintains the advertisements being displayed
 * on the index page.
 * 
 * @author Tyler Patricio
 */
@SuppressWarnings("serial")
@Named("adActionBean")
@SessionScoped
public class AdActionBean implements Serializable
{
	@Inject
	AdvertisementDAOImp adi;
	
	private int adCounter = 1;
	private final String FILE_PRE_FIX = "GOTAd";
	private final String EXTENSION = ".png";
	private AdvertisementBean adBean;
	// set URL as the first one by default
	private String externalUrl = "http://www.hbocanada.com/gameofthrones/";
	
	public AdActionBean()
	{
		super();
	}
	
	/**
	 * Displays a different advertisement upon refresh of page.
	 * 
	 * @throws SQLException 
	 */
	private String displayDifferentAd() throws SQLException
	{
		String fileName = "";
		
		if (adCounter == 4)
			adCounter = 1;
		
		fileName = FILE_PRE_FIX + adCounter + EXTENSION;
		adCounter++;
		
		ArrayList<AdvertisementBean> ab = adi.getSpecificQueryRecords(fileName);
		adBean = ab.get(0);
		
		externalUrl = adBean.getUrl();
		
		return adBean.getAdFileName();
	}

	/**
	 * Returns the file name.
	 * 
	 * @return the fileName
	 * @throws SQLException 
	 */
	public String getFileName() throws SQLException 
	{
		return displayDifferentAd();
	}

	/**
	 * @return the externalUrl
	 */
	public String getExternalUrl()
	{
		return externalUrl;
	}

	/**
	 * @param externalUrl the externalUrl to set
	 */
	public void setExternalUrl(String externalUrl) 
	{
		this.externalUrl = externalUrl;
	}
}