package com.g3w14.action;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.BookBean;
import com.g3w14.data.InvoiceBean;
import com.g3w14.data.InvoiceDetailBean;
import com.g3w14.data.ShippingInfoBean;
import com.g3w14.data.TaxBean;
import com.g3w14.persistence.InvoiceDAOImp;
import com.g3w14.persistence.InvoiceDetailDAOImp;
import com.g3w14.persistence.ShippingInfoDAOImp;
import com.g3w14.persistence.TaxDAOImp;

/**		CheckoutBean
 * 
 * This bean creates invoices and calculates the taxes depending on the billing information entered.
 * 
 * 
 * @author Sophie Leduc Major
 */
@SuppressWarnings("serial")
@Named("checkoutBean")
@ConversationScoped
public class CheckoutBean implements Serializable {

	@Inject
	private LoginBean cb;
	@Inject
	private InvoiceBean ib;
	@Inject
	private InvoiceDAOImp idi;
	@Inject
	private InvoiceDetailBean ibd;
	@Inject
	private InvoiceDetailDAOImp iddi;
	@Inject
	private ShippingInfoBean sib;
	@Inject
	private ShippingInfoDAOImp sidi;
	@Inject
	private CartBean cartbean;
	@Inject
	private TaxBean tb;
	@Inject
	private TaxDAOImp tbdi;

	private String invoice;

	private BigDecimal subtotal;

	private BigDecimal gst;
	private BigDecimal pst;
	private BigDecimal total;

	public CheckoutBean() {
		tb = new TaxBean();
		ib = new InvoiceBean();
	}

	public String getInvoice() {
		return invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getGst() {
		return gst;
	}

	public void setGst(BigDecimal gst) {
		this.gst = gst;
	}

	public BigDecimal getPst() {
		return pst;
	}

	public void setPst(BigDecimal pst) {
		this.pst = pst;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	/**		createInvoice
	 * This method creates the invoice the user can print.
	 * it is all in one string that contains the invoice details, the shipping and
	 * billing information, the total price of the books bought, etc.
	 * 
	 * @throws SQLException
	 */
	public void createInvoice() throws SQLException {
		
		//text will be the outputed invoice.
		String text = "";

		ib.setClientNumber(Integer.toString(cb.getClientBean().getId()));

		text = text + "Client Id: " + cb.getClientBean().getId() + " \n\n";

		Calendar c = new GregorianCalendar();
		Timestamp t = new Timestamp(0);
		t.setTime(c.getTimeInMillis());

		ib.setDate(t);

		t.setNanos(0);

		String provinceTax = cb.getClientBean().getProvince();

		tb = tbdi.getTaxByProvince(provinceTax.trim());

		calculateTaxes();
		
		ib.setIdpst(tb.getId());

		ib.setTotalNet(subtotal);

		ib.setTotalGross(total);

		//we need to create the invoice bean before th invoice detail to get the generated invoice id.
		idi.insertRecord(ib);
		
		createShippingEntry();

		ArrayList<InvoiceBean> invoices = idi.getInvoiceClient(cb
				.getClientBean().getEmail(), "", "");

		for (int i = 0; invoices.size() > i; i++) {

			//we get the invoice we just created.
			if (invoices.get(i).getDate().equals(t)) {

				ib = invoices.get(i);
			}
		}

		ArrayList<BookBean> books = cartbean.getShoppingCart();

		text = text + "Sale Number: " + ib.getSaleNumber() + " \n\n";

		text = text + "Billing Information  \n";
		text = text + "Name: " + cb.getClientBean().getFirstName() + " "
				+ cb.getClientBean().getLastName() + " \n";
		text = text + "Address 1: " + cb.getClientBean().getAddress1() + " \n";
		text = text + "Address 2: " + cb.getClientBean().getAddress2() + " \n";
		text = text + "City: " + cb.getClientBean().getCity() + " \n";
		text = text + "Province: " + cb.getClientBean().getProvince() + " \n";
		text = text + "Country: " + cb.getClientBean().getCountry() + " \n";
		text = text + "Postal Code: " + cb.getClientBean().getPostalCode()
				+ " \n\n";

		text = text + "Shipping Information  \n";
		text = text + "Name:  " + sib.getShipFirstName() + " "
				+ sib.getShipLastName() + " \n";
		text = text + "Address 1:  " + sib.getShipAddress1() + " \n";
		text = text + "Address 2:  " + sib.getShipAddress2() + " \n";
		text = text + "City:  " + sib.getShipCity() + " \n";
		text = text + "Province:  " + sib.getShipProvince() + " \n";
		text = text + "Country:  " + sib.getShipCountry() + " \n";
		text = text + "Postal Code:  " + sib.getShipPostalCode() + " \n\n";

		for (int i = 0; books.size() > i; i++) {
			ibd = new InvoiceDetailBean();
			ibd.setBookIsbn(books.get(i).getIsbn13());
			ibd.setBookPrice(books.get(i).getSalePrice());
			ibd.setQuantity(books.get(i).getQuantity());
			ibd.setSaleNumber(ib.getSaleNumber());

			text = text + "Book ISBN: " + ibd.getBookIsbn()
					+ "\nBook Quantity: " + ibd.getQuantity()
					+ "\nBook Price: " + ibd.getBookPrice() + " \n\n";

			//insert a invoice detail for every book.
			iddi.insertRecord(ibd);

		}
		
		calculateTaxes();

		text = text + "SubTotal: " + getSubtotal() + " \n";
		text = text + "GST: " + getGst() + " \n";
		text = text + "PST: " + getPst() + " \n";
		text = text + "Total: " + getTotal() + " \n";

		setInvoice(text);
		
		

	}
	
	/**		createShippingEntry
	 * After the invoice is created we save the shipping inforamtion entered for the clients later use
	 * for easy checkout.
	 * 
	 * @throws SQLException
	 */
	private void createShippingEntry() throws SQLException{
		
		String name = sib.getShipFirstName().substring(0, 1) + "." + sib.getShipLastName().substring(0, 1) + "., " + sib.getShipAddress1().substring(0, 5) + "...";
		
		ArrayList<ShippingInfoBean> shippingInfos = sidi.searchShipInfoByClientId(cb.getClientBean().getId());
		
		//if the shipping information enterd is already in the database, we dont want to create an other entry.
		boolean isMatch = false;
		for(int i = 0; shippingInfos.size() > i; i++){
			if(shippingInfos.get(i).getShipName() == name){
				isMatch = true;//if we find a match, we don't want an other shipping entry.
			}
		}
		
		//if we dont find a match, then we make a new entry.
		if(!isMatch){
			sib.setShipName(name);
			sib.setClientid(cb.getClientBean().getId());
	
			if(sib.getShipAddress2() == "" || sib.getShipAddress2() == null){
				sib.setShipAddress2("N/A");
			}
			
			
			sidi.insertShippingInfo(sib);
			}
		
	}
	
	
	/**		generateReceit
	 * Generates the receipt by calculating the taxes.
	 * 
	 * @throws SQLException
	 */
	public void generateReceit() throws SQLException {
		calculateTaxes();
	}

	/**		calculateTaxes
	 * 
	 * This calculates the taxes for the books bought.
	 * 
	 * @throws SQLException
	 */
	private void calculateTaxes() throws SQLException {

		setSubtotal(cartbean.getSubtotal());

		//get which province to get the appropriate tax percentage.
		String provinceTax = cb.getClientBean().getProvince();

		tb = tbdi.getTaxByProvince(provinceTax.trim());

		//since our taxs in the database are in whole values, we need to devide them by 100.
		//we multiply the subtotal by the gst
		// gst = subtotal * tax%
		setGst(subtotal.multiply(ib.getGST().divide(new BigDecimal("100")))
				.setScale(2, RoundingMode.CEILING));

		//same for the gst.
		setPst((subtotal.add(gst)).multiply(
				tb.getPst().divide(new BigDecimal("100"))).setScale(2,
				RoundingMode.CEILING));

		//we then add the subtotal, the gst and the pst.
		//total = subtotal + gst + pst.
		setTotal((subtotal.add(getPst())).add(getGst()));

	}

}
