package com.g3w14.action;

import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.ClientBean;
import com.g3w14.persistence.ClientDAO;
import com.g3w14.persistence.ClientDAOImp;

/**
 * Action Bean that handles adding the newly created user via. the 
 * sign-up page to the Client database.
 * 
 * @author Theo de Villeneuve, sophie
 *
 */

@Named("inscrireBean")
@RequestScoped
public class InscrireBean implements Serializable
{

	//email pattern
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\."
			+ "[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*"
			+ "(\\.[A-Za-z]{2,})$";

	private Pattern pattern;
	private Matcher matcher;


	private static final long serialVersionUID = 1L;
	// Injection 
	@Inject
	private ClientBean   clientbean;
	
	@Inject
	private ClientDAOImp clientdao;


	// Méthode d'action appelée lors du clic sur le bouton du formulaire
	// d'inscription
	public synchronized String inscrire() throws IOException, NoSuchAlgorithmException, 
	InvalidKeySpecException, SQLException {
		if(clientbean.getAddress2() == ""  |  clientbean.getAddress2() == null ){
			clientbean.setAddress2("N/C");
		}
		if(clientbean.getCompany() == "" | clientbean.getCompany() == null){
			clientbean.setCompany("N/C");
		}
		clientdao.insertClient(clientbean);
		return ("signupsuccess");
	}
	
	/**		validate
	 * 
	 * say if the Email already exist in the database and if it is valide
	 * 
	 */
	public void validateEmail(FacesContext context, UIComponent validate, Object value){
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(value.toString());
		if(!matcher.matches()){
			throw new ValidatorException(new FacesMessage(
					"Invalid email format"));
//            ((UIInput)validate).setValid(false);
//            FacesMessage msg = new FacesMessage("Invalid email");
//            context.addMessage(validate.getClientId(context), msg);
		}
		String email = (String)value;
		try {
			if(clientdao.clientExist(email)){
				throw new ValidatorException(new FacesMessage(
						"Email already exist"));
//	            ((UIInput)validate).setValid(false);
//	            FacesMessage msg = new FacesMessage("Email already exist");
//	            context.addMessage(validate.getClientId(context), msg);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		}
	}

}