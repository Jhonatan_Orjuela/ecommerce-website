package com.g3w14.action;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Cookie;

import com.g3w14.data.BookBean;
import com.g3w14.persistence.BookDAOImp;

/**
 * Action Bean class that adds and removes books from the user's shopping
 * cart.
 * 
 * @author Tyler Patricio, Sean-Frankel Gaon Canlas
 */
@SuppressWarnings("serial")
@Named("cartBean")
@SessionScoped
public class CartBean implements Serializable
{	
	@Inject
	private BookDAOImp bdi;
	
	private ArrayList<BookBean> shoppingCart;
	private int numOfBooksInCart = 0;
	private BigDecimal subtotal = new BigDecimal("0.00");
	private final int EXTRA_COPY = 1;
	
	public CartBean()
	{
		shoppingCart = new ArrayList<BookBean>();
	}
	
	/**
	 * @return the shoppingCart
	 */
	public ArrayList<BookBean> getShoppingCart()
	{
		return shoppingCart;
	}

	/**
	 * @param shoppingCart the shoppingCart to set
	 */
	public void setShoppingCart(ArrayList<BookBean> shoppingCart)
	{
		this.shoppingCart = shoppingCart;
	}
	
	/**
	 * @return the numOfBooksInCart
	 */
	public int getNumOfBooksInCart()
	{
		return numOfBooksInCart;
	}

	/**
	 * @param numOfBooksInCart the numOfBooksInCart to set
	 */
	public void setNumOfBooksInCart(int numOfBooksInCart)
	{
		this.numOfBooksInCart = numOfBooksInCart;
	}
	
	/**
	 * @return the subtotal
	 */
	public BigDecimal getSubtotal()
	{
		return subtotal;
	}

	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(BigDecimal subtotal)
	{
		this.subtotal = subtotal;
	}

	/**
	 * Adds a given book to the user's shopping cart.
	 * 
	 * @throws Exception
	 */
	public String addBookToCart(String isbn13) throws Exception
	{
		//String isbn13 = getParameter("isbn13");
		
		if (isbn13 == null)
			isbn13 = "";
		
		if (!isbn13.equals(""))
		{
			// check if the book is NOT already in the shopping cart
			if (!isAlreadyInCart(isbn13))
			{
				BookBean newlyAddedBook = bdi.getQuerySingleBook(isbn13);
				
				int quantity = newlyAddedBook.getQuantity();
				quantity += EXTRA_COPY;
				
				newlyAddedBook.setQuantity(quantity);

				// check if it is on sale and perform calculations with its sale price if true
				if (isOnSale(newlyAddedBook))
				{
					subtotal = subtotal.add(newlyAddedBook.getSalePrice());
					newlyAddedBook.setBookPricing(newlyAddedBook.getBookPricing().add(new BigDecimal(newlyAddedBook.getSalePrice() + "")));
				}
				
				// otherwise, resort to using its list price
				else
				{
					subtotal = subtotal.add(newlyAddedBook.getListPrice());
					newlyAddedBook.setBookPricing(newlyAddedBook.getBookPricing().add(new BigDecimal(newlyAddedBook.getListPrice() + "")));
				}
				
				shoppingCart.add(newlyAddedBook);
			}
			
			else
			{
				BookBean newlyAddedBook = bdi.getQuerySingleBook(isbn13);
				
				int size = shoppingCart.size();
				
				for (int i = 0; i < size; i++)
				{
					if (shoppingCart.get(i).getIsbn13().equals(newlyAddedBook.getIsbn13()))
					{	
						shoppingCart.get(i).setQuantity(shoppingCart.get(i).getQuantity() + 1);
						
						BigDecimal product;
						
						// update book price seeing as there are multiple copies of the same book
						if (isOnSale(shoppingCart.get(i)))
							product = new BigDecimal(newlyAddedBook.getSalePrice().multiply(new BigDecimal(shoppingCart.get(i).getQuantity() + "")) + "");

						else
							product = new BigDecimal(newlyAddedBook.getListPrice().multiply(new BigDecimal(shoppingCart.get(i).getQuantity() + "")) + "");
						
						shoppingCart.get(i).setBookPricing(product);
						
						// terminate loop early for efficiency
						i = size;
					}
				} // end loop
			}
		} // end main if
		
		return "toCart";
	}
	
	/**
	 * Empties the shopping cart after the user checkouts the books.
	 */
	public String emptyCart()
	{
		shoppingCart = new ArrayList<BookBean>();
		numOfBooksInCart = 0;
		subtotal = new BigDecimal("0.00");
		
		return "toHome";
	}
	
	/**
	 * Method that gets called whenever someone goes to cart.xhtml
	 */
	public void refreshCart() {
		numOfBooksInCart = calculateTotalBooks();
		calculateSubTotal();
		isQuantityZero();
	}

	/**
	 * Removes the specified book from the user's shopping cart.
	 * 
	 * @param isbn13
	 */
	public void removeBook(String isbn13)
	{	
		for (int i = 0; i < shoppingCart.size(); i++)
		{
			if (shoppingCart.get(i).getIsbn13().equals(isbn13))
			{
				BookBean bookToRemove = shoppingCart.get(i);

				subtotal = subtotal.subtract(new BigDecimal(bookToRemove.getBookPricing() + ""));
				numOfBooksInCart -= bookToRemove.getQuantity();

				bookToRemove.setQuantity(0);
				shoppingCart.remove(bookToRemove);

				// terminate loop early for efficiency
				i = shoppingCart.size();
			}
		}
	}

	/**
	 * Validates if the user's input satisfies the quantity on hand.
	 * 
	 * @param context
	 * @param validate
	 * @param value
	 * @throws Exception 
	 */
	public String validateQuantityInput(String isbn13) throws Exception
	{
		//String isbn13 = getParameter("isbn13");
		
		for (int i = 0; i < shoppingCart.size(); i++)
		{
			if (shoppingCart.get(i).getIsbn13().equals(isbn13))
			{
				BookBean book = bdi.getQuerySingleBook(isbn13);
				
				int inStock = book.getNumOfCopies();
				int quantity = shoppingCart.get(i).getQuantity();
				
				if (quantity > inStock)
					shoppingCart.get(i).setQuantity(inStock);
				
				BigDecimal product;
				
				// update book price seeing as there are multiple copies of the same book
				if (isOnSale(shoppingCart.get(i)))
					product = new BigDecimal(shoppingCart.get(i).getSalePrice().multiply(new BigDecimal(shoppingCart.get(i).getQuantity() + "")) + "");

				else
					product = new BigDecimal(shoppingCart.get(i).getListPrice().multiply(new BigDecimal(shoppingCart.get(i).getQuantity() + "")) + "");
				
				shoppingCart.get(i).setBookPricing(product);
			}
		}
		
		return "toCart";
	}
	
	/**
	 * Calculates the total number of books presently in the user's
	 * shopping cart.
	 * 
	 * @return
	 */
	private int calculateTotalBooks()
	{
		int totalBooks = 0;
		
		for (int i = 0; i < shoppingCart.size(); i++)
			totalBooks += shoppingCart.get(i).getQuantity();
		
		return totalBooks;
	}
	
	/**
	 * Dynamically calculates the sub-total.
	 */
	private void calculateSubTotal()
	{
		setSubtotal(new BigDecimal("0.00"));
		
		for (int i = 0; i < shoppingCart.size(); i++)
			subtotal = subtotal.add(new BigDecimal(shoppingCart.get(i).getBookPricing() + ""));
	}
	
	/*
	 * Method that checks if an quantity is set to 0, if they are it removes it.
	 */
	private void isQuantityZero() {
		for(int i = 0; i < shoppingCart.size(); i++) {
			if(shoppingCart.get(i).getQuantity() < 1)
				removeBook(shoppingCart.get(i).getIsbn13());
		}
	}
	
	/**
	 * Returns a given ISBN passed in from the f:param tag.
	 * 
	 * @return
	 */
	private String getParameter(String param)
	{
		String result = "";
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map<String,String> params = facesContext.getExternalContext().getRequestParameterMap();
		result = params.get(param);
		
		return result;
	}
	
	/**
	 * Verifies if the same book is already in the shopping cart.
	 * 
	 * @param isbn13
	 * @return
	 */
	private boolean isAlreadyInCart(String isbn13)
	{
		boolean result = false;
		
		for (int i = 0; i < shoppingCart.size(); i++)
		{
			BookBean bb = shoppingCart.get(i);
			
			if (bb.getIsbn13().equals(isbn13))
				result = true;
		}
		
		return result;
	}
	
	/**
	 * Verifies if a given book is on sale or not.
	 */
	private boolean isOnSale(BookBean book)
	{
		boolean result = false;
		
		// check if the book is on sale
		if (book.getSalePrice().compareTo(new BigDecimal(0.00)) > 0)
			result = true;
		
		return result;
	}
	
	/**
	 * Reads a specific cookie identified by a book's ISBN number.
	 * 
	 * @param isbn13
	 * @return
	 */
	private Cookie readCookie(String isbn13)
	{
		Cookie cartedBookCookie = 
				(Cookie)FacesContext.getCurrentInstance().getExternalContext().getRequestCookieMap().get(isbn13 + "QuantityCookie");
		
		// set max age of cookie to one day
		cartedBookCookie.setMaxAge(86400);
		
		return cartedBookCookie;
	}
	
	/**
	 * Creates a specific cookie identified by a book's ISBN number.
	 * 
	 * @param isbn13
	 * @return
	 */
	private void writeCookie(String isbn13)
	{
		FacesContext context = FacesContext.getCurrentInstance();
		//context.getExternalContext().addResponseCookie(isbn13 + "QuantityCookie", String.valueOf(quantity), null);
	}
}