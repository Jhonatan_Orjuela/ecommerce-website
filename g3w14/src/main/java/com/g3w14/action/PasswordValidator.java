package com.g3w14.action;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validates the format of a given password.
 * 
 * @author Théo de villeneuve
 *
 */
@FacesValidator(value = "passwordValidator")
public class PasswordValidator implements Validator 
{
	//password pattern
	private static final String PASSWORD_PATTERN = 
			"((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,78})";

	private Pattern pattern;
	private Matcher matcher;

	//	private ResourceBundle messages = null;

	public PasswordValidator() {
		pattern = Pattern.compile(PASSWORD_PATTERN);
	}

	/**		validate
	 * 
	 * This validates a password the user enters and verifies if the password respects the rules (regex).
	 * If not, it sends a message saying the password is invalid.
	 * 
	 */
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		matcher = pattern.matcher(value.toString());
		String password1 = (String) component.getAttributes().get("password1");
		
		System.out.println("Component thing: " + component.getAttributes().get("password1"));
		
		if(!matcher.matches())
		{
			FacesMessage msg = 
					new FacesMessage("Password validation failed.", 
							"Password (length 6-78) must contains : one digit, one lowercase characters, one lowercase characters, one special symbols in the list (@#$%)");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);

		}
	}
}