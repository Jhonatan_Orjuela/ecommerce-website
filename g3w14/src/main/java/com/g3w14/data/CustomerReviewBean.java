package com.g3w14.data;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Bean class that denotes all of the variables relating to the
 * CustomerReview table.
 * 
 * @author Sean-Frankel Gaon Canlas, Tyler, Theo
 *
 */
@Named("customerReviewBean")
@RequestScoped
public class CustomerReviewBean implements Serializable {

	String isbn = "";
	String text ="";
	String name ="";
	int clientId = 0;
	int rating;
	boolean approval;
	int key;
	Timestamp date;
	
	public CustomerReviewBean() {
		super();
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public int getClientId() {
		return clientId;
	}
	
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean getApproval() {
		return approval;
	}

	public void setApproval(boolean approval) {
		this.approval = approval;
	}
	
	public int getKey() {
		return key;
	}
	
	public void setKey(int key) {
		this.key = key;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomerReviewBean [isbn=" + isbn + ", text=" + text
				+ ", name=" + name + ", clientId=" + clientId + ", rating="
				+ rating + ", approval=" + approval + ", key=" + key
				+ ", date=" + date + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (approval ? 1231 : 1237);
		result = prime * result + clientId;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		result = prime * result + key;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + rating;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerReviewBean other = (CustomerReviewBean) obj;
		if (approval != other.approval)
			return false;
		if (clientId != other.clientId)
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		if (key != other.key)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rating != other.rating)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
}