package com.g3w14.data;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Bean class that denotes all of the variables relating to the
 * InvoiceDetail table.
 * 
 * @author 1410616
 *
 */
@Named("invoiceDetailBean")
@RequestScoped
public class InvoiceDetailBean implements Serializable
{
	private int id;
	private int saleNumber;
	private int quantity;
	private String bookIsbn;
	private BigDecimal bookPrice;
	
	/**
	 * No-parameter constructor.
	 */
	public InvoiceDetailBean()
	{
		id = 0;
		saleNumber = 0;
		quantity = 0;
		bookIsbn = "";
		bookPrice = new BigDecimal(-1.00);
	}
	
	/**
	 * Parameter-filled constructor.
	 * 
	 * @param id
	 * @param saleNumber
	 * @param quantity
	 * @param bookIsbn
	 * @param bookPrice
	 */
	public InvoiceDetailBean(int id, int saleNumber, int quantity, String bookIsbn,
			BigDecimal bookPrice)
	{
		this.id = id;
		this.saleNumber = saleNumber;
		this.quantity = quantity;
		this.bookIsbn = bookIsbn;
		this.bookPrice = bookPrice;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the saleNumber
	 */
	public int getSaleNumber() {
		return saleNumber;
	}
	/**
	 * @param saleNumber the saleNumber to set
	 */
	public void setSaleNumber(int saleNumber) {
		this.saleNumber = saleNumber;
	}
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the bookIsbn
	 */
	public String getBookIsbn() {
		return bookIsbn;
	}
	/**
	 * @param bookIsbn the bookIsbn to set
	 */
	public void setBookIsbn(String bookIsbn) {
		this.bookIsbn = bookIsbn;
	}
	/**
	 * @return the bookPrice
	 */
	public BigDecimal getBookPrice() {
		return bookPrice;
	}
	/**
	 * @param bookPrice the bookPrice to set
	 */
	public void setBookPrice(BigDecimal bookPrice) {
		this.bookPrice = bookPrice;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bookIsbn == null) ? 0 : bookIsbn.hashCode());
		result = prime * result
				+ ((bookPrice == null) ? 0 : bookPrice.hashCode());
		result = prime * result + id;
		result = prime * result + quantity;
		result = prime * result + saleNumber;
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceDetailBean other = (InvoiceDetailBean) obj;
		if (bookIsbn == null) {
			if (other.bookIsbn != null)
				return false;
		} else if (!bookIsbn.equals(other.bookIsbn))
			return false;
		if (bookPrice == null) {
			if (other.bookPrice != null)
				return false;
		} else if (!bookPrice.equals(other.bookPrice))
			return false;
		if (id != other.id)
			return false;
		if (quantity != other.quantity)
			return false;
		if (saleNumber != other.saleNumber)
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "InvoiceDetailBean [id=" + id + ", saleNumber=" + saleNumber
				+ ", quantity=" + quantity + ", bookIsbn=" + bookIsbn
				+ ", bookPrice=" + bookPrice + "]";
	}
}