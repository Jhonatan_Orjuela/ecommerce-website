package com.g3w14.testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.g3w14.data.ClientBean;
import com.g3w14.persistence.ClientDAOImp;

/**
 * JUnit testing cases for ClientDAOImp class.
 * 
 * @author Sophie Leduc Major (0931442)
 * 
 */
@RunWith(Arquillian.class)
public class ClientDAOImpTest {
	@Inject
	ClientDAOImp cdao;

	@Deployment
	public static WebArchive deploy()
	{
		final File[] dependencies = Maven
				.resolver()
				.loadPomFromFile("pom.xml")
				.resolve("mysql:mysql-connector-java",
						"org.assertj:assertj-core").withoutTransitivity()
				.asFile();

		final WebArchive webArchive = ShrinkWrap.create(WebArchive.class)
				.addPackage(ClientDAOImp.class.getPackage())
				.addPackage(ClientBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("resources-mysql-ds.xml", "resources.xml")
				.addAsLibraries(dependencies);

		return webArchive;
	}
	
	@Before
	public void init() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
		ClientBean c = new ClientBean();
		cdao = new ClientDAOImp();

		c.setId(1);
		c.setEmail("herp@derp.com");
		c.setFirstName("sean");
		c.setLastName("bob");
		
		cdao.updateClient(c);

	}

	@Test
	public void testQueryAllClients() throws SQLException {
		
		assertEquals(2, cdao.getQueryClients().size());

	}

	@Test
	public void testQuerySingleClientNormal() throws SQLException {
		
		assertEquals("sean", cdao.getQuerySingleClient("herp@derp.com").getFirstName());
	}

	@Test
	public void testQuerySingleClientNoSuchClient() throws SQLException {

		assertNull(cdao.getQuerySingleClient("no"));
	}

	@Test
	public void testInsertClientNormal() throws NoSuchAlgorithmException, InvalidKeySpecException, SQLException {
		ClientBean b = new ClientBean();
		b.setEmail("home@idk.com");
		b.setFirstName("Joe");
		b.setLastName("Ewok");
		b.setPassword("isuck");
		
		assertEquals(1, cdao.insertClient(b));
		
		cdao.deleteClient(cdao.getQuerySingleClient("home@idk.com").getId());
		

	}

	@Test (expected=SQLException.class)
	public void testInsertClientExisitingEmail() throws NoSuchAlgorithmException, InvalidKeySpecException, SQLException {
		
		ClientBean b = new ClientBean();
		b.setEmail("herp@derp.com");
		b.setFirstName("Joe");
		b.setLastName("Ewok");
		b.setPassword("isuck");
		cdao.insertClient(b);
		

	}

	@Test
	public void testUpdateClientNormal() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
		
		ClientBean b = new ClientBean();
		b.setId(1);
		b.setFirstName("Joe");
		b.setLastName("Ewok");
		b.setPassword("isuck");
		b.setEmail("abc@def.com");
		cdao.insertClient(b);
		
		ClientBean d = new ClientBean();
		d.setFirstName("john");
		int id = cdao.getQuerySingleClient("abc@def.com").getId();
		d.setId(id);
		
		
		
		assertEquals(1, cdao.updateClient(d));
		
		cdao.deleteClient(id);
		

	}

	@Test
	public void testUpdateClientNoSuchClient() throws SQLException {

		ClientBean b = new ClientBean();
		b.setId(999);
		b.setFirstName("Joe");
		b.setLastName("Ewok");
		b.setPassword("isuck");
		assertEquals(0,cdao.updateClient(b));
	}

	@Test
	public void testDeleteClientNormal() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException{
		ClientBean b = new ClientBean();
	
		b.setFirstName("Joe");
		b.setLastName("Ewok");
		b.setPassword("isuck");
		b.setEmail("ha@ho.com");
		cdao.insertClient(b);
		int id = cdao.getQuerySingleClient("ha@ho.com").getId();
		
		assertEquals(1, cdao.deleteClient(id));

	}

	@Test 
	public void testDeleteClientNoSuchClient() throws SQLException {
		assertEquals(0, cdao.deleteClient(999));
	}

	@Test
	public void testUpdatePasswordNormal() throws NoSuchAlgorithmException, InvalidKeySpecException, SQLException {
		ClientBean b = new ClientBean();
		b.setFirstName("Joe");
		b.setLastName("Ewok");
		b.setPassword("isuck");
		b.setEmail("nope@yes.com");
		b.setPassword("poop");
		cdao.insertClient(b);
		
		ClientBean d = new ClientBean();
		int id = cdao.getQuerySingleClient("nope@yes.com").getId();
		d.setId(id);
		d.setPassword("boob");
		
		assertEquals(1, cdao.updateClientPassword(d));
		
		cdao.deleteClient(id);
	}

	@Test
	public void testUpdatePasswordNoSuchClient() throws NoSuchAlgorithmException, InvalidKeySpecException, SQLException {
		ClientBean b = new ClientBean();
		b.setId(999);
		b.setPassword("boob");
		assertEquals(0,cdao.updateClientPassword(b));
	}

	@Test
	public void testClientExist() throws SQLException {
		String email = "herp@derp.com";
		boolean isExistent = cdao.clientExist(email);
		
		assertEquals("True expected here.", true, isExistent);
	}
	
	@Test
	public void testClientExistWithNonExistingEmail() throws SQLException {
		String email = "tyler@tyler.com";
		boolean isExistent = cdao.clientExist(email);
		
		assertEquals("False expected here.", false, isExistent);
	}
}