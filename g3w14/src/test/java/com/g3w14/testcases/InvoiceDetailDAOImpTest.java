package com.g3w14.testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.g3w14.data.InvoiceDetailBean;
import com.g3w14.persistence.InvoiceDetailDAOImp;

/**
 * JUnit testing cases for InvoiceDetailDAOImp class.
 * 
 * @author Théo
 *
 */
@RunWith(Arquillian.class)
public class InvoiceDetailDAOImpTest {

	@Inject
	InvoiceDetailDAOImp iddi;
	InvoiceDetailBean idb;
	
	ArrayList<InvoiceDetailBean> idbList;

	@Deployment
	public static WebArchive deploy()
	{
		final File[] dependencies = Maven
				.resolver()
				.loadPomFromFile("pom.xml")
				.resolve("mysql:mysql-connector-java",
						"org.assertj:assertj-core").withoutTransitivity()
				.asFile();

		final WebArchive webArchive = ShrinkWrap.create(WebArchive.class)
				.addPackage(InvoiceDetailDAOImp.class.getPackage())
				.addPackage(InvoiceDetailBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("resources-mysql-ds.xml", "resources.xml")
				.addAsLibraries(dependencies);

		return webArchive;
	}
	
	@Before
	public void init() throws SQLException
	{
		idb = new InvoiceDetailBean();
		idbList = new ArrayList<InvoiceDetailBean>();
	}	

	/**
	 * Process required to re-create the book table before every test.
	 */
	@Before
	public void recreateDB() throws SQLException
	{

		final String URL = "jdbc:mysql://waldo2.dawsoncollege.qc.ca:3306/g3w14";
		final String USER = "g3w14";
		final String PASSWORD = "sofa4brick";		

		String dropTableQuery = "DROP TABLE if exists  invoicedetail, invoice, taxes";

		String createTableTaxes = "CREATE TABLE  taxes (" +
				"id integer NOT NULL AUTO_INCREMENT," +
				"province varchar(2) not null," +
				"pst decimal(4,2) NOT NULL," +
				"PRIMARY KEY (id)" +
				") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		String createTableInvoice = "CREATE TABLE  invoice (" +
				"salenumber mediumint(11) NOT NULL AUTO_INCREMENT," +
				"dateofsale datetime NOT NULL," +
				"clientnumber integer(11) NOT NULL," +
				"totalnet decimal(3,2) DEFAULT NULL," +
				"idpst integer NOT NULL," +
				"gst decimal(3,2) NOT NULL DEFAULT 5.00," +
				"totalgross decimal(4,2)," +
				"FOREIGN KEY (idpst) REFERENCES taxes(id) on delete cascade," +
				"PRIMARY KEY (salenumber)" +
				") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		String createTableInvoiceDetail = "CREATE TABLE  invoicedetail (" +
				"id mediumint(11) NOT NULL AUTO_INCREMENT," +
				"salenumber mediumint(11) NOT NULL," +
				"quantity integer(3) NOT NULL," +
				"bookisbn varchar(14) NOT NULL," +
				"bookprice decimal(3,2) NOT NULL," +
				"PRIMARY KEY (id)," +
				"FOREIGN KEY (salenumber) REFERENCES invoice(salenumber) on delete cascade" +
				") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
		PreparedStatement pStatement = connection.prepareStatement(dropTableQuery);
		PreparedStatement pStatement4 = connection.prepareStatement(createTableTaxes);
		PreparedStatement pStatement2 = connection.prepareStatement(createTableInvoice);
		PreparedStatement pStatement3 = connection.prepareStatement(createTableInvoiceDetail);


		{
			pStatement.execute();
			pStatement4.execute();
			pStatement2.execute();
			pStatement3.execute();

		}
		String insertQuery = "INSERT INTO invoice (salenumber, dateofsale, clientnumber, totalnet, idpst, gst, totalgross)" +
				"VALUES (1, '2014-01-23 10:56:35', 12,'5.00' , 1, '5.00','10.00');";

		String insertQueryTaxe = "INSERT INTO taxes (province, pst)" +
				"VALUES ('PN','5.00');";
		String insertQuery2 ="INSERT INTO invoicedetail (id, salenumber, quantity, bookisbn, bookprice)"+
				"VALUES (1, 1, 12,'978-0261102354' , '6.00');";
		
		try (PreparedStatement pStatement5 = connection
				.prepareStatement(insertQuery);
				PreparedStatement pStatement6 = connection
						.prepareStatement(insertQueryTaxe);
				PreparedStatement pStatement7 = connection
						.prepareStatement(insertQuery2);) {
			pStatement6.executeUpdate();
			pStatement5.executeUpdate();
			pStatement7.executeUpdate();
		}
	}


	/**
	 * Test method for {@link com.g3w14.persistence.InvoiceDetailDAOImp#getQueryRecords()}.
	 * @throws SQLException 
	 */
	@Test
	public void testGetQueryRecords() throws SQLException {
		idbList = iddi.getQueryRecords();

		// check current records amount
		assertEquals("Total records in database: 1", 1, idbList.size());

	}

	/**
	 * Test method for {@link com.g3w14.persistence.InvoiceDetailDAOImp#extractSingleInvoiceDetail(int)}.
	 * @throws SQLException 
	 */
	@Test
	public void testExtractSingleInvoiceDetail() throws SQLException {

		idb = iddi.extractSingleInvoiceDetail(1);

		// check ib's id
		assertEquals("Record id expected now : 1", 1, idb.getSaleNumber());
	}
	/**
	 * Test method for {@link com.g3w14.persistence.InvoiceDetailDAOImp#extractSingleInvoiceDetail(int)}.
	 * @throws SQLException 
	 */
	@Test
	public void testExtractSingleInvoiceDetailWithFalseNumber() throws SQLException {

		idb = iddi.extractSingleInvoiceDetail(6);

		// check ib's id
		assertEquals("Record id expected now : none", 0, idb.getSaleNumber());
	}

	/**
	 * Test method for {@link com.g3w14.persistence.InvoiceDetailDAOImp#insertRecord(com.g3w14.data.InvoiceDetailBean)}.
	 * @throws SQLException 
	 */
	@Test
	public void testInsertRecord() throws SQLException {
		idbList = iddi.getQueryRecords();

		// check to see the basic size
		assertEquals("Records expected now: 1", 1, idbList.size());

		// check current records amount

		idb.setSaleNumber(2);
		idb.setId(2);
		idb.setSaleNumber(1);
		idb.setQuantity(3);
		idb.setBookIsbn("978-0261102354");
		idb.setBookPrice(new BigDecimal(5.00));

		iddi.insertRecord(idb);

		idbList = iddi.getQueryRecords();

		// check to see if list got bigger
		assertEquals("Records expected now: 2", 2, idbList.size());

	}

	/**
	 * Test method for {@link com.g3w14.persistence.InvoiceDetailDAOImp#updateRecord(com.g3w14.data.InvoiceDetailBean)}.
	 * @throws SQLException 
	 */
	@Test
	public void testUpdateRecord() throws SQLException {
		int resultNum;
		idbList = iddi.getQueryRecords();
		idbList.get(0).setBookPrice(new BigDecimal(3));
		resultNum = iddi.updateRecord(idbList.get(0));
		assertEquals("The number 1 is expected here:", 1, resultNum);
	}
	/**
	 * Test method for {@link com.g3w14.persistence.InvoiceDetailDAOImp#deleteRecord(int)}.
	 * @throws SQLException 
	 */
	@Test
	public void testDeleteRecord() throws SQLException {
		int resultNum;
		idb = iddi.extractSingleInvoiceDetail(1);
		resultNum = iddi.deleteRecord(idb);

		assertEquals("The number 1 is expected here:", 1, resultNum);
	}
	/**
	 * Test method for {@link com.g3w14.persistence.InvoiceDetailDAOImp#deleteRecord(int)}.
	 * @throws SQLException 
	 */
	@Test//(timeout=2000)
	public void testDeleteRecordWithFalseInvoice() throws SQLException {
		int resultNum;
		idb = iddi.extractSingleInvoiceDetail(5);
		resultNum = iddi.deleteRecord(idb);

		assertEquals("The number 0 is expected here:", 0, resultNum);
	}

}
