package com.g3w14.testcases;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BookDAOImpTest.class, ClientDAOImpTest.class,
		CustomerReviewImpTest.class, InvoiceDAOImpTest.class,
		InvoiceDetailDAOImpTest.class, PasswordPBKDF2Test.class,
		SurveyDAOImpTest.class, TaxDAOImpTest.class, AdvertisementDAOImpTest.class })
public class AllTests {

}
