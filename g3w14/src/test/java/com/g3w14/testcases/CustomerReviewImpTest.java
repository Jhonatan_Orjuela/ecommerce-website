package com.g3w14.testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.g3w14.data.CustomerReviewBean;
import com.g3w14.persistence.CustomerReviewDAOImp;

/**
 * This is the JUnit for the Customer Review table.
 * @author Sean-Frankel Gaon Canlas
 *
 */
@RunWith(Arquillian.class)
public class CustomerReviewImpTest {

	@Inject
	private CustomerReviewDAOImp crbDAO;
	
	private CustomerReviewBean crb;
	private ArrayList<CustomerReviewBean> crbList;

	private final String url = "jdbc:mysql://waldo2.dawsoncollege.qc.ca:3306/g3w14";
	private final String user = "g3w14";
	private final String pass = "sofa4brick";

	@Deployment
	public static WebArchive deploy()
	{
		final File[] dependencies = Maven
				.resolver()
				.loadPomFromFile("pom.xml")
				.resolve("mysql:mysql-connector-java",
						"org.assertj:assertj-core").withoutTransitivity()
				.asFile();

		final WebArchive webArchive = ShrinkWrap.create(WebArchive.class)
				.addPackage(CustomerReviewDAOImp.class.getPackage())
				.addPackage(CustomerReviewBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("resources-mysql-ds.xml", "resources.xml")
				.addAsLibraries(dependencies);

		return webArchive;
	}
	
	@Before
	public void init() throws SQLException {
		crb = new CustomerReviewBean();
		crbList = new ArrayList<CustomerReviewBean>();

		String dropTable = "DROP TABLE IF EXISTS customerreview";
		String sql = "CREATE TABLE IF NOT EXISTS customerreview ("
				+ "reviewkey integer NOT NULL auto_increment,"
				+ "isbn varchar(14) NOT NULL,"
				+ "date datetime NOT NULL,"
				+ "clientid integer NOT NULL,"
				+ "rating integer(1) DEFAULT NULL,"
				+ "text varchar(750) NOT NULL,"
				+ "approval integer(1) NOT NULL,"
				+ "FOREIGN KEY (isbn) REFERENCES book(isbn13) on delete cascade,"
				+ "FOREIGN KEY (clientid) REFERENCES client(id) on delete cascade,"
				+ "PRIMARY KEY (reviewkey)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		try (Connection connection = DriverManager.getConnection(url, user,
				pass);
				PreparedStatement ps = connection.prepareStatement(dropTable);
				PreparedStatement ps2 = connection.prepareStatement(sql)) {
			ps.execute();
			ps2.execute();
		} catch (SQLException sqlex) {
			System.out.println(sqlex);
		}

		crb.setApproval(true);
		crb.setIsbn("978-0261102354");
		GregorianCalendar cal = new GregorianCalendar();
		crb.setDate(new Timestamp(cal.getTimeInMillis()));
		crb.setRating(10);
		crb.setClientId(1);
		crb.setText("Middle Earth? What is this middle earth you speak of? I live in Westeros. I am a Lannister and a Lannister always pays their debts. And I own Aragorn.");

		crbDAO.insertRecord(crb);

	}

	@Test
	public void testGetAllRecords() throws SQLException {
		crbList = crbDAO.getQueryRecords();
		assertEquals("This should be 1", 1, crbList.size());
		System.out.println(crbList.get(0).toString());
	}

	@Test
	public void testInsertOneRecord() throws SQLException {
		crb = new CustomerReviewBean();
		crb.setApproval(true);
		crb.setIsbn("978-0261102354");
		GregorianCalendar cal = new GregorianCalendar();
		crb.setDate(new Timestamp(cal.getTimeInMillis()));
		crb.setRating(10);
		crb.setClientId(1);
		crb.setText("LANNISTER.");

		int num = crbDAO.insertRecord(crb);

		assertEquals(1, num);
	}

	@Test
	public void testUpdateRecord() throws SQLException {
		crb.setApproval(true);
		crb.setIsbn("978-0261102354");
		GregorianCalendar cal = new GregorianCalendar();
		crb.setDate(new Timestamp(cal.getTimeInMillis()));
		crb.setRating(10);
		crb.setText("JAIME LANNISTER");

		int num = crbDAO.insertRecord(crb);

		assertEquals(1, num);
	}

	@Test
	public void testDeleteRecord() throws SQLException {
		crb.setKey(1);

		int num = crbDAO.deleteRecord(crb);

		assertEquals(1, num);
	}
	
	@Test
	public void testDeleteRecordWithException() throws SQLException {
		crb.setKey(2);
		crbDAO.deleteRecord(crb);
		fail("Should not work because there is no reviewkey of value 2.");
	}

}
