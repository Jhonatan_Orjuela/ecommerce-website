-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Sam 25 Janvier 2014 à 23:43
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--final String URL = "jdbc:mysql://waldo2.dawsoncollege.qc.ca:3306/g3w14";
--final String USER = "g3w14";
--final String PASSWORD = "sofa4brick";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: bookbaydata
--
CREATE DATABASE IF NOT EXISTS bookbaydata DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE g3w14;

-- --------------------------------------------------------

show tables from g3w14;
--
-- Structure de la table book
--

DROP TABLE IF EXISTS book;

select * from book;
CREATE TABLE IF NOT EXISTS book (
  isbn10 varchar(10) NOT NULL,
  isbn13 varchar(14) NOT NULL,
  title varchar(100) NOT NULL,
  author varchar(30) NOT NULL,
  publisher varchar(50) NOT NULL,
  numberofpages integer(4) NOT NULL,
  genre varchar(15) NOT NULL,
  image varchar(100) NOT NULL,
  booktype integer(1) NOT NULL,
  ebookformats varchar(35) NOT NULL,
  numberofcopies integer(7) NOT NULL,
  wholesaleprice decimal(5,2) NOT NULL,
  listprice decimal(5,2) NOT NULL,
  saleprice decimal(5,2) NOT NULL,
  weight integer(4) NOT NULL,
  dimensions varchar(20) NOT NULL,
  dateentered datetime NOT NULL,
  removalstatus integer(1) NOT NULL,
  PRIMARY KEY (isbn13)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

select * from book;
--
-- Contenu de la table book
--

delete from book;

INSERT INTO book (isbn10, isbn13, title, author, publisher, numberofpages, genre, image, booktype, ebookformats, numberofcopies, wholesaleprice, listprice, saleprice, weight, dimensions, dateentered, Removalstatus) VALUES
('0007123825', '978-0261102354', 'The Fellowship of the Ring', 'John Ronald Reuel Tolkien', 'George Allen & Unwin', 531, 'Fantasy', 'TheFellowshipOfTheRing_img.png', 3, 'DjVu', 0, '4.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('006083482X', '978-0060834821', 'The Sheltering Sky', 'Paul Bowles', 'Ecco', 352, 'Fiction', 'TheShelteringSky_img.png', 1, 'N/A', 12, '9.99', '9.99', '9.99', 420, '20.5 x 13.5 x 2.2', '2014-01-23 00:00:00', 0),
('0060883286', '978-0060883287', 'One Hundered Years of Solitude', 'Gabriel Garcia Marquez', 'Harper Perennial', 448, 'Fiction', '100YearsOfSolitude_img.png', 2, 'N/A', 10, '9.99', '9.99', '9.99', 522, '21.2 x 13.5 x 2.8', '2014-01-23 10:56:35', 0),
('0060889667', '978-0060889661', 'Survival of the Sickest', 'Sharon Moalem, Jonathan Prince', 'William Morrow', 288, 'Non-Fiction', 'SurvivalOfTheSickest_img.png', 3, 'e-Reader', 0, '4.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0060929804', '978-0060929800', 'Native Son', 'Richard Wright', 'Harper Perennial', 544, 'Fiction', 'NativeSon_img.png', 1, 'N/A', 15, '9.99', '9.99', '9.99', 431, '2.4 x 13.3 x 20.3', '2014-01-25 17:42:10', 0),
('0062293583', '978-0062293589', 'The Son', 'Philipp Meyer', 'HarperCollins Publishers Ltd', 576, 'Fiction', 'theSon_img.png', 1, 'n/a', 20, '3.00', '9.99', '9.99', 680, '22.8 x 15.6 x 3.8', '2014-01-23 10:56:36', 0),
('0064471837', '978-0064471831', 'Sabriel', 'Garth Nix', 'HarperCollins Publishers', 491, 'Fantasy', 'Sabriel_img.png', 3, 'Kindle', 0, '3.89', '7.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0099458195', '978-0099458197', 'Point Counter Point', 'Aldous Huxley', 'Vintage Classics', 464, 'Fiction', 'PointCounterPoint_img.png', 1, 'N/A', 3, '9.99', '9.99', '9.99', 399, '13.2 x 19.7', '2014-01-25 17:42:10', 0),
('0099478420', '978-0099478423', 'The Heart of the Matter', 'Graham Greene', 'Vintage Classics', 272, 'Speculative Fic', 'TheHeartOfTheMatter_img.png', 1, 'N/A', 20, '9.99', '9.99', '9.99', 181, '13.2 x 19.7', '2014-01-25 17:42:10', 0),
('0140042598', '978-0140042597', 'On The Road', 'Jack Kerouac', 'Penguin Books', 320, 'Fiction', 'onTheRoad_img.png', 1, 'N/A', 10, '9.99', '9.99', '9.99', 222, '19.4 x 12.8 x 1.8', '2014-01-23 00:00:00', 0),
('0140177396', '978-0749717100', 'Of Mice and Men', 'John Steinbeck', 'Penguin Books', 112, 'Fiction', 'OfMiceAndMen_img.png', 1, 'N/A', 10, '5.50', '9.99', '9.89', 45, '19.3 x 11.1 x 0.8', '2014-01-23 10:56:35', 0),
('0141036133', '978-0141036137', 'Animal Farm', 'George Orwell', 'Penguin UK', 112, 'Fiction', 'AnimalFarm_img.png', 1, 'N/A', 10, '5.50', '9.99', '9.89', 82, '1 x 10.8 x 17.3', '2014-01-23 10:56:35', 0),
('0141182601', '978-0141182605', 'A Clockwork Orange', 'Anthony Burgess', 'Penguin Modern Classics', 176, 'Science fiction', 'AClockworkOrange_img.png', 1, 'N/A', 25, '9.99', '9.99', '9.99', 141, '19.8 x 13 x 1', '2014-01-23 00:00:00', 0),
('0143039431', '978-0143039433', 'The Grapes of Wrath', 'John Steinbeck', 'Penguin Classics', 544, 'Fiction', 'TheGrapesOfWrath_img.png', 1, 'N/A', 10, '9.00', '9.99', '9.99', 340, '2.5 x 12.7 x 19.3', '2014-01-23 10:56:35', 0),
('014318704X', '978-0143187042', 'The Silent Wife', 'Eleanor Catton', 'Penguin Canada', 336, 'Fiction', 'TheSilentWife_img.png', 1, 'N/A', 10, '8.99', '9.99', '9.00', 218, '20.6 x 13.2 x 2.5', '2014-01-23 10:56:35', 0),
('0156907399', '978-0156907392', 'To the Lighthouse', 'Virginia Woolf', 'Houghton Mifflin Harcourt', 224, 'Fiction', 'ToTheLightHouse_img.png', 1, 'N/A', 8, '9.99', '9.99', '9.99', 249, '20.6 x 13.5 x 1.4', '2014-01-25 17:43:17', 0),
('0199538107', '978-0199538102', 'The House of Mirth', 'Edith Wharton', 'Oxford Paperbacks', 368, 'Fiction', 'theHouseofMirth_img.png', 1, 'N/A', 35, '8.95', '9.99', '9.86', 259, '19.3 x 12.7 x 1.8', '2014-01-23 00:00:00', 0),
('0199695156', '978-0199695157', 'Finnegans Wake', 'James Joyce', 'Oxford University Press', 720, 'Fiction', 'FinnegansWake_img.png', 1, 'N/A', 10, '9.99', '9.99', '9.99', 499, '19.4 x 12.8 x 3.6', '2014-01-23 00:00:00', 0),
('0261102214', '978-0261102217', 'The Hobbit', 'John Ronald Reuel Tolkien', 'George Allen & Unwin', 310, 'Fantasy', 'TheHobbit_img.png', 3, 'Mobi-Pocket', 0, '5.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0261102362', '978-0261102361', 'The Two Towers', 'John Ronald Reuel Tolkien', 'George Allen & Unwin', 416, 'Fantasy', 'TheTwoTowers_img.png', 3, 'PDF', 0, '4.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:36', 0),
('0261102370', '978-0261102378', 'The Return of the King', 'John Ronald Reuel Tolkien', 'George Allen & Unwin', 624, 'Fantasy', 'TheReturnOfTheKing_img.png', 3, 'ePub', 0, '4.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('030735654X', '978-0307356543', 'Brave New World', 'Aldous Huxley', 'Vintage Canada', 272, 'Science Fiction', 'braveNewWorld_img.png', 2, 'N/A', 10, '5.00', '9.99', '9.99', 295, '20.1 x 12.7 x 2.3', '2014-01-25 15:45:56', 0),
('0307387895', '978-0307387899', 'The Road', 'Cormac McCarthy', 'Vintage', 304, 'Fiction', 'theRoad_img.png', 2, 'N/A', 16, '3.00', '9.99', '8.00', 295, '2.2 x 13 x 20.1', '2014-01-25 15:45:56', 0),
('0307474275', '978-0307474278', 'The Da Vinci Code', 'Dan Brown', 'Anchor', 624, 'Mystery', 'daVinciCode_img.png', 1, 'n/a', 15, '4.00', '9.99', '9.99', 762, '3.3 x 10.7 x 18.9', '2014-01-23 10:56:36', 0),
('0307743667', '978-0307743664', 'Carrie', 'Stephen King', 'Anchor', 304, 'Horror', 'Carrie_img.png', 1, 'N/A', 14, '5.00', '9.99', '9.49', 159, '2.3 x 10.5 x 17', '2014-01-23 00:00:00', 0),
('0307743683', '978-0307743688', 'The Stand', 'Stephen King', 'Anchor', 1472, 'Post apocalypti', 'TheStand_img.png', 1, 'N/A', 32, '6.00', '9.99', '9.89', 621, '5.3 x 10.8 x 18.4', '2014-01-23 00:00:00', 0),
('0312858477', '978-0312858476', 'Trader', 'Charles de Lint', 'Tor Books', 352, 'Fantasy', 'Trader_img.png', 2, 'N/A', 3, '9.99', '9.99', '9.99', 590, '23.8 x 15.4 x 3.2', '2014-01-25 17:43:17', 0),
('0316055433', '978-0316055437', 'The Goldfinch', 'Donna Tartt', 'Little, Brown and Company', 784, 'Fiction', 'TheGoldfinch_img.png', 2, 'N/A', 10, '9.99', '9.99', '9.99', 1100, '23.9 x 16 x 4.6', '2014-01-23 10:56:35', 0),
('0316769487', '978-0316769488', 'The Catcher in the Rye ', 'J.D. Salinger', 'Little, Brown and Company', 224, 'Fiction', 'TheCatcherInTheRye_img.png', 1, 'N/A', 10, '3.99', '7.99', '5.58', 141, '17 x 10.8 x 1.5', '2014-01-23 10:56:35', 0),
('0375701966', '978-0375701962', 'The Moviegoer', 'Walker Percy', 'Vintage', 256, 'Fiction', 'TheMovieGoer_img.png', 1, 'N/A', 5, '9.99', '9.99', '9.99', 358, '20.3 x 13.2 x 1.3', '2014-01-25 17:43:17', 0),
('0380729407', '978-0380729401', 'Something Wicked This Way Comes', 'Ray Bradbury', 'Harper Voyager', 304, 'Supernatural', 'SomethingWickedThisWayComes_img.png', 1, 'N/A', 58, '5.00', '8.99', '8.54', 113, '2.2 x 10.5 x 17.1', '2014-01-23 00:00:00', 0),
('038531387X', '978-0385313872', 'Deliverance', 'James Dickey', 'Delta', 288, 'Fiction', 'Deliverance_img.png', 1, 'N/A', 10, '9.99', '9.99', '9.99', 295, '20.3 x 13.5 x 1.7', '2014-01-25 17:42:10', 0),
('0385678150', '978-0385678155', 'The Encyclopedia of Early Earth', 'Isabel Greenberg', 'Bond Street Books', 176, 'Fantasy', 'TheEncyclopediaOfEarlyEarth_img.png', 2, 'N/A', 10, '9.99', '9.99', '9.99', 1000, '29.8 x 21.2 x 2.2', '2014-01-23 10:56:35', 0),
('0385720173', '978-0385720175', 'The Death of the Heart', 'Elizabeth Bowen', 'Anchor', 432, 'melodramatic', 'TheDeathoftheHeart_img.png', 1, 'N/A', 6, '9.99', '9.99', '9.99', 297, '20.4 x 13.3 x 2.4', '2014-01-23 00:00:00', 0),
('0385743564', '978-0385743563', 'SteelHeart', 'Brandon Sanderson', 'Delacorte Press', 400, 'Fiction', 'Steelheart_img.png', 2, 'N/A', 10, '9.99', '9.99', '9.99', 612, ' 23.4 x 15.2 x 3.6', '2014-01-23 10:56:36', 0),
('0399159347', '978-0399159343', 'The Husband''s Secret', 'Liane Moriarty', 'Amy Einhorn Books/Putnam', 416, 'Fiction', 'TheHusbandsSecret_img.png', 2, 'N/A', 10, '9.99', '9.99', '9.99', 612, '23.9 x 16 x 4.3', '2014-01-23 10:56:35', 0),
('0439023521', '978-0439023528', 'The Hunger Games', 'Suzanne Collins', 'Scholastic Press', 384, 'Action & Advent', 'HungerGames_img.png', 1, 'N/A', 10, '5.99', '9.99', '7.18', 318, '20.3 x 2 x 13.3', '2014-01-23 10:56:35', 0),
('0440180295', '978-0440180296', 'Slaughterhouse-Five', 'Kurt Vonnegut Jr.', 'Dell', 224, 'Dark comedy', 'slaughterHouseFive_img.png', 1, 'n/a', 10, '5.00', '9.99', '7.00', 59, '59, 1.5 x 10.6 x 17.', '2014-01-23 10:56:36', 0),
('0449015777', '978-0449015773', 'Ottolenghi: The Cookbook', 'Yotam Ottolenghi', 'Appetite By Random House', 304, 'Cookbooks, Food', 'OttolenghiTheCookbook_img.png', 2, 'N/A', 10, '9.99', '9.99', '9.99', 1300, '27.2 x 20 x 3.4', '2014-01-23 10:56:35', 0),
('0451457811', '978-0451457813', 'Storm Front', 'Jim Butcher', 'Penguin Putnam', 322, 'Fantasy', 'StormFront_img.png', 3, 'PDF', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0451458125', '978-0451458124', 'Fool Moon', 'Jim Butcher', 'Penguin Putnam', 358, 'Fantasy', 'FoolMoon_img.png', 3, 'PDF', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0451458443', '978-0451458445', 'Grave Peril', 'Jim Butcher', 'Penguin Putnam', 421, 'Fantasy', 'GravePeril_img.png', 3, 'ePub', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0451458923', '978-0451458926', 'Summer Knight', 'Jim Butcher', 'Penguin Putnam', 322, 'Fantasy', 'SummerKnight_img.png', 3, 'DjVu', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0451459407', '978-0451459404', 'Death Masks', 'Jim Butcher', 'Penguin Putnam', 458, 'Fantasy', 'DeathMasks_img.png', 3, 'Kindle', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0451459873', '978-0451459879', 'Blood Rites', 'Jim Butcher', 'Penguin Putnam', 587, 'Fantasy', 'BloodRites_img.png', 3, 'Kindle', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('045146091X', '978-0451460912', 'Dead Beat', 'Jim Butcher', 'Penguin Putnam', 486, 'Fantasy', 'DeadBeat_img.png', 3, 'Kindle', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0451461037', '978-0451461032', 'Proven Guilty', 'Jim Butcher', 'Penguin Putnam', 398, 'Fantasy', 'ProvenGuilty_img.png', 3, 'Mobi-Pocket', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('045146155X', '978-0451461551', 'White Night', 'Jim Butcher', 'Penguin Putnam', 458, 'Fantasy', 'WhiteNight_img.png', 3, 'PDF', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0451462009', '978-0451462008', 'Small Favors', 'Jim Butcher', 'Penguin Putnam', 423, 'Fantasy', 'SmallFavors_img.png', 3, 'PDF', 0, '5.50', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0451524934', '978-0451524935', '1984', 'George Orwell', 'Signet Classics', 336, 'Fiction', '1984_img.png', 2, 'N/A', 10, '5.00', '9.99', '7.00', 113, '2.4 x 10.2 x 18.4', '2014-01-25 15:45:11', 0),
('0451531558', '978-0451531551', 'An American Tragedy', 'Theodore Dreiser', 'Signet Classics', 896, 'Fiction', 'AnAmericanTragedy_img.png', 1, 'N/A', 3, '7.00', '9.99', '9.99', 386, '3.9 x 11.1 x 17.1', '2014-01-25 17:42:10', 0),
('0486264645', '978-0486264646', 'Heart of Darkness', 'Joseph Conrad', 'Dover Publications', 80, 'Fiction', 'HeartOfDarkness_img.png', 1, 'N/A', 10, '2.00', '4.00', '3.25', 68, '20.6 x 13.2 x 0.8', '2014-01-25 17:43:17', 0),
('0486411095', '978-0486411095', 'Dracula', 'Bram Stoker', 'Archibald Constable and Company', 200, 'Horror', 'Dracula_img.png', 3, 'Mobi-Pocket', 0, '4.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0486434664', '978-0486434667', 'The Way of All Flesh', 'Samuel Butler', 'Dover Publications', 320, 'Autobiography', 'WayofAllFlesh_img.png', 1, 'N/A', 5, '5.25', '8.00', '6.75', 249, '21 x 13.2 x 2', '2014-01-25 17:42:10', 0),
('0486436659', '978-0486436654', 'David Copperfield', 'Charles Dickens', 'Dover Publications', 736, 'Fiction', 'davidCopperfield_img.png', 2, 'N/A', 10, '2.00', '9.00', '6.00', 522, '4.4 x 13.2 x 20.6', '2014-01-25 15:45:56', 0),
('0515138819', '978-0515138818', 'Sunshine', 'Robin McKinley', 'Berkley Publishing Group', 450, 'Fantasy', 'Sunshine_img.png', 3, 'e-Reader', 0, '0.45', '0.90', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0553214519', '978-0553214512', 'Main Street', 'Sinclair Lewis', 'Bantam Classics', 624, 'fiction', 'MainStreet_img.png', 1, 'N/A', 65, '6.99', '6.99', '6.64', 281, '2.3 x 10.8 x 17.8', '2014-01-23 00:00:00', 0),
('0571084834', '978-0571084838', 'Lord of The Flies', 'William Golding', 'Faber And Faber Ltd.', 240, 'Fiction', 'LordOfTheFlies_img.png', 1, 'N/A', 10, '5.99', '9.99', '9.99', 136, '17.8 x 10.9 x 1.3', '2014-01-23 10:56:35', 0),
('0618706410', '978-0618706419', 'The Things They Carried', 'Tim O''Brien', 'Houghton Mifflin Harcourt', 256, 'War stories', 'thingsTheyCarried_img.png', 2, 'N/A', 10, '5.00', '9.99', '9.99', 272, '20.2 x 13.3 x 1.7', '2014-01-25 15:45:57', 0),
('0670024783', '978-0670024780', 'The Invention of Wings', 'Sue Monk Kidd', 'Viking Adult', 384, 'Fiction', 'TheInventionOfWings_img.png', 2, 'N/A', 20, '9.99', '9.99', '9.99', 680, '24.5 x 16.3 x 3.1', '2014-01-25 17:43:17', 0),
('0670024856', '978-0670024858', 'The Signature of All Things : A Novel', 'Elizabeth Gilbert', 'Viking Adult', 512, 'Fiction', 'TheSignatureOfAllThingsANovel_img.png', 2, 'N/A', 10, '9.99', '9.99', '9.99', 748, '15.2 x 22.9', '2014-01-23 10:56:35', 0),
('0670067512', '978-0670067510', 'And the mountains Echoed', 'Khaled Hosseini', 'Viking Canada', 416, 'Fiction', 'AndTheMountainsEchoed_img.png', 2, 'N/A', 10, '9.99', '9.99', '9.99', 726, '23.6 x 16.5 x 3', '2014-01-23 10:56:35', 0),
('0679722645', '978-0679722649', 'The Maltese Falcon', 'Dashiell Hammett', 'Vintage', 224, 'Detective ficti', 'TheMalteseFalcon_img.png', 1, 'N/A', 155, '9.99', '9.99', '9.99', 224, '1.5 x 13.8 x 20.6', '2014-01-23 00:00:00', 0),
('0679728759', '978-0679728757', 'Blood Meridian: Or the Evening Redness in the West', 'Cormac McCarthy', 'Vintage', 368, 'Western', 'bloodMeridian_img.png', 2, 'N/A', 10, '5.00', '9.99', '9.99', 386, '20.4 x 13.3 x 1.9', '2014-01-25 15:45:56', 0),
('0679732241', '978-0679732242', 'The Sound and the Fury', 'William Faulkner', 'Vintage', 368, 'Fiction', 'soundAndFury_img.png', 1, 'n/a', 10, '5.00', '9.99', '7.00', 91, '2.3 x 12.8 x 20', '2014-01-23 10:56:36', 0),
('067973225X', '978-0679732259', 'As I Lay Dying', 'William Faulkner', 'Vintage', 288, 'Fiction', 'AsILayDying_img.png', 1, 'N/A', 18, '9.99', '9.99', '9.99', 159, '1.5 x 13.1 x 19.4', '2014-01-25 17:42:10', 0),
('0679732764', '978-0679732761', 'Invisible Man', 'Ralph Ellison', 'Vintage', 608, 'Fiction', 'InvisibleMan_img.png', 1, 'N/A', 7, '9.99', '9.99', '9.99', 227, '20.3 x 13.7 x 3', '2014-01-25 17:42:10', 0),
('0684801221', '978-0684801223', 'Old Man and the Sea', 'Ernest Hemingway', 'Scribner', 128, 'Fiction', 'OldManAndTheSea_img.png', 1, 'N/A', 15, '8.95', '9.99', '9.99', 113, '0.7 x 13.8 x 20', '2014-01-25 17:43:16', 0),
('0743273567', '978-0743273565', 'The Great Gatsby', 'F.Scott Fitzgerald', 'Scribner', 192, 'Fiction', 'TheGreatGatsby_img.png', 1, 'N/A', 10, '8.50', '9.99', '9.99', 113, '20.1 x 13.2 x 1.3', '2014-01-23 10:56:35', 0),
('0743297334', '978-0743297332', 'The Sun Also Rises', 'Ernest Hemingway', 'Scribner', 256, 'Fiction', 'TheSunAlsoRises_img.png', 1, 'N/A', 5, '9.95', '9.99', '9.99', 204, '13.5 x 1.3 x 20.8', '2014-01-25 17:42:10', 0),
('0765365278', '978-0765365279', 'The Way of Kings', 'Brandon Sanderson', 'Tor Books', 1001, 'Fantasy', 'TheWayOfKings_img.png', 3, 'ePub', 0, '4.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0771019106', '978-0771019104', 'The Luminaries', 'Eleanor Catton', 'McClelland & Stewart', 848, 'Fiction', 'TheLuminaries_img.png', 2, 'N/A', 10, '9.99', '9.99', '9.99', 1100, '23.2 x 15.4 x 5.8', '2014-01-23 10:56:35', 0),
('0836218256', '978-0590062275', 'Something Under The Bed Is Drooling', 'Bill Watterson', 'Andrews McMeel Publishing', 127, 'Comic Strips', 'SomethingUnderTheBedIsDrooling_img.png', 3, 'DjVu', 0, '6.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('0930289234', '978-0930289232', 'Watchmen', 'Alan Moore', 'DC Comics', 272, 'Science Fiction', 'TheWatchmen_img.png', 3, 'Mobi-Pocket', 0, '4.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('1401219217', '978-1401219215', 'Y The Last Man Deluxe Edition Book One', 'Brian K. Vaughan', 'Vertigo', 256, 'Post-apocalypti', 'YTheLastManDeluxeEditionBookOne_img.png', 2, 'N/A', 87, '9.99', '9.99', '9.99', 816, '2.2 x 19.1 x 28.6', '2014-01-23 00:00:00', 0),
('1416524797', '978-1416524793', 'Angels & Demons', 'Dan Brown', 'Pocket Books', 736, 'Mystery', 'angelsDemons_img.png', 1, 'N/A', 5, '3.00', '9.99', '9.00', 272, '3.7 x 10.4 x 18.8', '2014-01-23 10:56:36', 0),
('1416524800', '978-1416524809', 'Deception Point', 'Dan Brown', 'Pocket Books', 752, 'Thriller', 'deceptionPoint_img.png', 1, 'N/A', 17, '3.00', '9.99', '9.99', 386, '19.3 x 10.4 x 4.8', '2014-01-23 10:56:36', 0),
('1439195269', '978-1439195260', 'Lonesome Dove: A Novel', 'Larry McMutry', 'Simon & Schuste', 864, 'Western', 'lonesomeDove_img.png', 1, 'N/A', 12, '4.00', '9.99', '9.99', 680, '4.6 x 13.3 x 20.3', '2014-01-23 10:56:36', 0),
('1443422665', '978-1443422666', 'The Rosie Project', 'Graeme Simsion', 'HarperCollins Publishers Ltd.', 288, 'Fiction', 'TheRosieProject_img.png', 1, 'N/A', 10, '9.99', '9.99', '9.99', 363, '22.4 x 14.2 x 2.8', '2014-01-23 10:56:35', 0),
('1443422908', '978-1443422901', 'Orfeo', 'Richard Powers', 'HarperCollins Publishers Ltd', 384, 'Fiction', 'Orfeo_img.png', 1, 'N/A', 14, '9.99', '9.99', '9.99', 900, '19.3 x 12.7 x 1.8', '2014-01-23 00:00:00', 0),
('1476743940', '978-1476743943', 'Under The Dome', 'Stephen King', 'Gallery Books', 1088, 'Science Fiction', 'underTheDome_img.png', 1, 'n/a', 10, '5.00', '9.99', '9.99', 1300, '22.8 x 15.4 x 5.2', '2014-01-23 10:56:36', 0),
('1568650965', '978-1568650968', 'Litany of the Long Sun', 'Gene Wolfe', 'Tor Books', 845, 'Fantasy', 'LitanyOfTheLongSun_img.png', 3, 'ePub', 0, '7.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('1582348286', '978-1582348285', 'Harry Potter and the Philosophers Stone', 'J.K. Rowling', 'Bloomsbury Publishing PLC', 300, 'Fiction', 'harryPotterPhilostone_img.png', 1, 'n/a', 20, '2.00', '9.99', '9.99', 295, '20.1 x 14 x 3', '2014-01-23 10:56:36', 0),
('1607060760', '978-1607060765', 'The Walking Dead Compendium Volume 1', 'Robert Kirkman', 'Image Comics', 1088, 'Post-apocalypti', 'TheWalkingDeadCompendiumVolume1_img.png', 1, 'N/A', 48, '9.99', '9.99', '9.99', 2100, '5 x 16.5 x 25.5', '2014-01-23 00:00:00', 0),
('1770890327', '978-1770890329', 'The Sisters Brothers', 'Patrick Dewitt', 'House of Anansi Press', 344, 'Fiction', 'theSistersBrothers_img.png', 1, 'N/A', 20, '4.00', '9.99', '9.99', 386, '22.6 x 15.2 x 2.8', '2014-01-23 10:56:36', 0),
('1770892141', '978-1770892149', 'Ablutions', 'Patrick Dewitt', 'House of Anansi Press', 176, 'Fiction', 'ablutions_img.png', 1, 'N/A', 10, '4.00', '9.99', '9.99', 227, '20.8 x 13.8 x 1.4', '2014-01-23 10:56:36', 0),
('1840226358', '978-1840226355', 'Ulysses', 'James Joyce', 'Wordsworth Classics', 682, 'Modernist', 'ulysses_img.png', 1, 'N/A', 15, '2.00', '7.00', '5.00', 682, '4 x 12.3 x 19', '2014-01-23 10:56:36', 0),
('2701156319', '978-2701156316', 'Farenheit 451', 'Ray Bradbury', 'GALLIMARD (ÉDITIONS)', 179, 'Dystopian novel', 'farenheit_img.png', 2, 'N/A', 10, '2.00', '9.50', '6.50', 200, '17.4 x 12.4 x 1 ', '2014-01-25 15:45:56', 0),
('2922145441', '978-2922145441', 'Aliss', 'Patrick Senecal ', 'Alire', 450, 'Fantasy', 'Aliss_img.png', 3, 'e-Reader', 0, '4.99', '9.99', '0.00', 0, 'N/A', '2014-01-23 10:56:37', 0),
('374500010', '978-0374500016', 'Night', 'Elie Wiesel', 'FSG Adult', 144, 'Autobiography', 'night_img.png', 1, 'N/A', 20, '2.00', '2.00', '9.00', 136, '21.6 x 1.3 x 14', '2014-01-23 10:56:36', 0),
('375706674', '978-0375706677', 'No Country for Old Men', 'Cormac McCarthy', 'Vintage', 320, 'Thriller', 'noCountryOldMen_img.png', 1, 'N/A', 9, '3.00', '9.99', '9.99', 249, '13.2 x 1.6 x 20.2', '2014-01-23 10:56:36', 0),
('385537859', '978-0385537858', 'Inferno: A Novel', 'Dan Brown', 'Doubleday', 480, 'Mystery', 'inferno_img.png', 2, 'N/A', 6, '9.99', '9.99', '9.99', 998, '24.1 x 4.4 x 16.5', '2014-01-23 10:56:36', 0),
('440245117', '978-0440245117', 'The Confession', 'John Grisham', 'Dell', 528, 'Fiction', 'theConfession_img.png', 1, 'N/A', 15, '3.00', '9.99', '9.99', 386, '3.3 x 10.3 x 18.8', '2014-01-23 10:56:36', 0),
('446310786', '978-0446310789', 'To Kill A Mockingbird', 'Harper Lee', 'Grand Central Publishing', 384, 'Fiction', 'killMockingBird_img.png', 1, 'N/A', 21, '3.00', '8.99', '6.99', 181, '17 x 10.4 x 2.8', '2014-01-23 10:56:36', 0),
('486280616', '978-0486280615', 'Adventures of Huckleberry Finn', 'Mark Twain', 'Dover Publications', 224, 'Fiction', 'huckleberryFinn_img.png', 1, 'N/A', 10, '1.00', '6.00', '4.00', 181, '1.5 x 13 x 20.3', '2014-01-23 10:56:36', 0),
('486424537', '978-0486424538', 'Oliver Twist', 'Charles Dickens', 'Dover Publications', 362, 'Historical Fict', 'oliverTwist_img.png', 1, 'N/A', 5, '1.00', '4.75', '3.00', 45, '2.3 x 13 x 20.5', '2014-01-23 10:56:36', 0),
('553573403', '978-0553573404', 'A Game of Thrones: A Song of Ice and Fire: Book One', 'George R. R. Martin', 'Bantam', 864, 'Fantasy', 'gameOfThrones_img.png', 1, 'N/A', 50, '3.00', '9.99', '9.99', 340, '3.5 x 10.8 x 17.8', '2014-01-23 10:56:36', 0),
('743424425', '978-0743424424', 'The Shining', 'Stephen King', 'Pocket Books', 704, 'Horror', 'theShining_img.png', 1, 'N/A', 6, '4.00', '9.99', '9.99', 318, '2.8 x 10.2 x 17.8', '2014-01-23 10:56:36', 0),
('8983923393', '978-8983923394', 'The Lost Symbol', 'Dan Brown', 'Munhak Sucheob', 403, 'Mystery', 'theLostSymbol_img.png', 1, 'N/A', 12, '9.99', '9.99', '9.99', 590, '2.3 x 14.8 x 21.8', '2014-01-23 10:56:36', 0),
('9582701048', '978-0679723165', 'Lolita', 'Vladimir Nabokov', 'Vintage', 336, 'Fiction', 'Lolita_img.png', 1, 'N/A', 10, '9.50', '9.99', '9.99', 272, '1.9 x 13.1 x 20', '2014-01-23 10:56:35', 0),
('9780062024', '978-0062024039', 'Divergent', 'Veronica Roth', 'Katherine Tegen', 496, 'Fantasy', 'divergent_img.png', 1, 'N/A', 40, '2.00', '9.99', '6.50', 408, '3.1 x 13 x 20', '2014-01-23 10:56:36', 0),
('9781451626', '978-1451626650', 'Catch-22', 'Kurt Vonnegut Jr.', 'Simon & Schuster', 544, 'Fiction', 'Catch-22_img.png', 1, 'N/A', 10, '9.50', '9.99', '9.99', 454, '3.4 x 13.8 x 21.3', '2014-01-23 10:56:35', 0);


-- --------------------------------------------------------

--
-- Structure de la table client
--

DROP TABLE IF EXISTS client;

CREATE TABLE IF NOT EXISTS client (
  id integer NOT NULL AUTO_INCREMENT,
  email varchar(50) UNIQUE,
  password varchar(78),
  title varchar(3),
  lastname varchar(20) NOT NULL,
  firstname varchar(15) NOT NULL,
  company varchar(15),
  address1 varchar(25),
  address2 varchar(25),
  city varchar(30),
  province varchar(3),
  country varchar(6),
  postalcode varchar(7),
  hometelephone varchar(12),
  celltelephone varchar(12),
  lastsearch varchar(15), 
  admin integer(1) default 0,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


insert into client(email, password, title, lastname, firstname, company, address1, city, province, country, postalcode, shiptitle, shiplastname, shipfirstname, shipcompany, shipaddress1, shipaddress2)



select * from client;
UPDATE client SET admin = 1
WHERE id = 1;

insert into client(lastname, firstname) values ("Lannister", "Tyrion");
insert into client(email,firstname,lastname) values ("herp@derp.com", "sean", "bob");
insert into client(id, email, password, title, lastname, firstname, company, address1, address2, city, province, country, postalcode,hometelephone, celltelephone, lastsearch) values(0,"sophie.leduc.major@gmail.com", "123", "Ms", "Sophie", "Leduc Major",  "BookBay", "1234 road", "5678 street", "Montreal", "QC", "Canada", "1A2B3C","2111111111", "1111111111", "");



-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS shipinfo (
  id integer NOT NULL AUTO_INCREMENT,
  shipname varchar(15) NOT NULL,
  shiptitle varchar(3),
  shiplastname varchar(20),
  shipfirstname varchar(15),
  shipcompany varchar(15),
  shipaddress1 varchar(25),
  shipaddress2 varchar(25),
  shipcity varchar(30),
  shipprovince varchar(2),
  shipcountry varchar(6),
  shippostalcode varchar(7),
  clientid integer NOT NULL,
    FOREIGN KEY shipinfo(clientid) REFERENCES client(id) on delete cascade,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



--
-- Structure de la table customerreviews
--

DROP TABLE IF EXISTS customerreview;
CREATE TABLE IF NOT EXISTS customerreview (
  reviewkey integer NOT NULL auto_increment,
  isbn varchar(14) NOT NULL,
  date datetime NOT NULL,
  clientid integer NOT NULL,
  rating integer(1) DEFAULT NULL,
  text varchar(750) NOT NULL,
  approval boolean NOT NULL,
  name varchar(50) not null,
  FOREIGN KEY (isbn) REFERENCES book(isbn13) on delete cascade,
  FOREIGN KEY (clientid) REFERENCES client(id) on delete cascade,
  PRIMARY KEY (reviewkey)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


select * from customerreview;
-- --------------------------------------------------------

--
-- Structure de la table invoice
--
CREATE TABLE IF NOT EXISTS invoice (
  salenumber integer(11) NOT NULL AUTO_INCREMENT,
  dateofsale datetime NOT NULL,
  clientnumber integer(11) NOT NULL,
  totalnet decimal(6,2) DEFAULT NULL,
  idpst integer NOT NULL,
  gst decimal(3,2) NOT NULL DEFAULT 5.00,
  totalgross decimal(6,2),
  FOREIGN KEY (idpst) REFERENCES taxes(id) on delete cascade,
  FOREIGN KEY (clientnumber) REFERENCES client(id) on delete cascade,
  PRIMARY KEY (salenumber)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


select * from invoicedetail;

INSERT INTO invoice ( dateofsale, clientnumber, totalnet, idpst, gst, totalgross) VALUES (NOW() , 1, '99.99',2,'5.00' ,'39.00');
INSERT INTO invoice ( dateofsale, clientnumber, totalnet, idpst, gst, totalgross) VALUES (NOW() , 1, '123.00',2,'5.00' ,'30.67');
INSERT INTO invoice ( dateofsale, clientnumber, totalnet, idpst, gst, totalgross) VALUES (NOW() , 1, '16.00',2,'5.00' ,'23.01');
INSERT INTO invoice ( dateofsale, clientnumber, totalnet, idpst, gst, totalgross) VALUES (NOW() , 1, '10.00',2,'5.00' ,'5.60');
INSERT INTO invoice ( dateofsale, clientnumber, totalnet, idpst, gst, totalgross) VALUES (NOW() , 2, '99.99',2,'5.00' ,'39.00');
INSERT INTO invoice ( dateofsale, clientnumber, totalnet, idpst, gst, totalgross) VALUES (NOW() , 2, '123.00',2,'5.00' ,'30.67');
INSERT INTO invoice ( dateofsale, clientnumber, totalnet, idpst, gst, totalgross) VALUES (NOW() , 2, '16.00',2,'5.00' ,'23.01');
INSERT INTO invoice ( dateofsale, clientnumber, totalnet, idpst, gst, totalgross) VALUES (NOW() , 2, '10.00',2,'5.00' ,'5.60');

INSERT INTO customerreview ( isbn, date, clientid, rating, text, approval, name) VALUES ('978-1607060765', NOW() , 1, 5,'very good very good ',TRUE, 'THEO HENRY DE VILLENEUVE');
INSERT INTO customerreview ( isbn, date, clientid, rating, text, approval, name) VALUES ('978-1607060765', NOW() , 1, 5,'dddddddddddddddddddllllllllllllll llllllllllllll dddddddddddddddd dddddddd ',true,'THEO HENRY DE VILLENEUVE');
INSERT INTO customerreview ( isbn, date, clientid, rating, text, approval, name) VALUES ('978-1607060765', NOW() , 1, 5,'yoppiyop',false,'THEO HENRY DE VILLENEUVE');
INSERT INTO customerreview ( isbn, date, clientid, rating, text, approval, name) VALUES ('978-1607060765', NOW() , 2, 5,'dddddddddddddddddddllllllllllllll llllllllllllll dddddddddddddddd dddddddd ',true,'paul henry');
INSERT INTO customerreview ( isbn, date, clientid, rating, text, approval, name) VALUES ('978-1607060765', NOW() , 2, 5,'yoppiyop',false,'paul henry');



select * from client;

drop table if exists invoicedetail, invoice, customerreview, shipinfo, client, book;

-- --------------------------------------------------------
--
-- Structure de la table invoiceClient
--


INSERT INTO invoicedetail (id, salenumber, quantity, bookisbn, bookprice) VALUES (1 , 1, 12,'978-0261102354' ,'6.00');
DROP TABLE invoicedetail;
CREATE TABLE IF NOT EXISTS invoicedetail (
  id integer(11) NOT NULL AUTO_INCREMENT,
  salenumber integer(11) NOT NULL,
  quantity integer(3) NOT NULL,
  bookisbn varchar(14) NOT NULL,
  bookprice decimal(5,2) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (bookisbn) REFERENCES book(isbn13) on delete cascade,
  FOREIGN KEY (salenumber) REFERENCES invoice(salenumber) on delete cascade
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

select * from invoicedetail;

INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (1, 1,'978-0261102354' ,'16.00');
INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (3, 3,'978-0261102354' ,'16.00');
INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (2, 1,'978-0141182605' ,'10.00');
INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (3, 1,'978-0141182605' ,'10.00');
INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (1, 1,'978-1568650968' ,'12.00');
INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (4, 1,'978-0261102354' ,'16.00');
INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (4, 1,'978-0515138818' ,'96.00');

INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (5, 1,'978-0451458445' ,'10.00');
INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (1, 5,'978-0451458926' ,'12.00');
INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (5, 1,'978-0451458445' ,'10.00');
INSERT INTO invoicedetail (salenumber, quantity, bookisbn, bookprice) VALUES (1, 5,'978-0451458926' ,'12.00');


SELECT book.isbn10, book.isbn13, book.title, book.author, book.publisher,
book.numberofpages, book.genre, book.image, book.booktype, book.ebookformats,
book.numberofcopies, book.wholesaleprice, book.listprice, book.saleprice, 
book.weight, book.dimensions, book.dateentered, book.Removalstatus ,
count(invoicedetail.bookisbn) c FROM book, invoicedetail, invoice WHERE book.isbn13 = invoicedetail.bookisbn GROUP BY invoicedetail.bookisbn ORDER BY c DESC limit 10;

-- --------------------------------------------------------



--
-- Structure de la table survey
--
select * from survey;
CREATE TABLE IF NOT EXISTS survey (
  surveyid integer(11) NOT NULL AUTO_INCREMENT,
  question varchar(150) NOT NULL,
  option1 varchar(100) NOT NULL,  
  votes1 integer(5) DEFAULT 0,
  option2 varchar(100) NOT NULL,
  votes2 integer(5) DEFAULT 0,
  option3 varchar(100) NOT NULL,
  votes3 integer(5) DEFAULT 0,
  option4 varchar(100) NOT NULL,
  votes4 integer(5) DEFAULT 0,
  totalvotes integer(11) DEFAULT 0,
  PRIMARY KEY (surveyid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Structure de la table taxes
--
select * from taxes;	
DROP TABLE invoicedetail, invoice, taxes;
CREATE TABLE IF NOT EXISTS taxes (
  id integer NOT NULL AUTO_INCREMENT,
  province varchar (2) NOT NULL,
  pst decimal(4,2) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO taxes (province,pst) VALUES
('AB','0.00'),
('BC','7.00'),
('MB','8.00'),
('NB','8.00'),
('NL','8.00'),
('NT','0.00'),
('NS','10.00'),
('NU','0.00'),
('ON','8.00'),
('PE','9.00'),
('QC','9.97'),
('SK','5.00'),
('YT','0.00');



--
-- Structure de la table advertisements
-- 
CREATE TABLE IF NOT EXISTS advertisements (
  adid integer NOT NULL AUTO_INCREMENT,
  adfilename varchar (50) NOT NULL,
  url varchar (255) NOT NULL ,
  PRIMARY KEY (adId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE advertisements ADD UNIQUE (url);

select * from advertisements;


INSERT INTO advertisements(adfilename, url)  VALUES("Chicken4Gold.jpg", "http://exchangeChicken4Gold.com");

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
